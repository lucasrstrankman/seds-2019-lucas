Data is from 431 Open Source Apps from the Google Play store, collected by the SEDS laboratory.
Work done in SEDS-Lucas was based off of work done in SEDS-Inderpreet. 


"RQ1.py", "RQ2.py", and "RQ3.py" were written in Python 3.7.4.
"extract_feature_dates.py" can be run in 32-bit Python 2.7.17 with the modules from "requirements.txt"

To extract the features Review and commit dates by topic, run "extract_feature_dates.py"
The folder "feature_dates" have the dates of extracted feature commits
The folder "feature_groups" have the dates of extracted feature requests from reviews

"RQ3.py" Finds common feature topics from the commits and reviews for each app.
Each topic is sorted by date. It then displays the number of topics that had a feature request
come before the feature commit for that topic.


"encodingToCategorization.py" and "categorizationToSynthesize.py" were used to process the output of
the GWM support tool for its next steps