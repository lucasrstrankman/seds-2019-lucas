invalidRegexes = ("(A*B*C*)*", "(A*C*B*)*", "(B*A*C*)*", "(B*C*A*)*", "(C*A*B*)*", "(C*B*A*)*") # This is because Synth pkg can't handle these, even though categorization pkg creates it

def convert(path):
    info = []
    with open(path) as f:
        info = [line.rstrip('\n') for line in f]
    
    info = [line.split('   ') for line in info]
    patterns = {}
    for line in info:
        if line[1] in patterns.keys():
            val = patterns[line[1]]
            patterns[line[1]] = val + " " + line[-1]
        elif line[1] not in invalidRegexes:
            patterns[line[1]] = line[-1]

    output = ""
    for pattern in patterns:
        output = output + pattern + " " + patterns[pattern] + '\n'

    with open("categorization-Processed.txt", 'w') as f: 
        f.write(output)

def main():
    
    fileName = "2020-03-06 11-08-01 encodingOutput output_log.csv"
    convert(fileName)

if __name__ == "__main__":
    main()