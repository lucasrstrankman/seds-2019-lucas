#!/usr/bin/env

import csv
import datetime
import os
import sys
import timeit
import threading

from PyQt5 import QtWidgets
from PyQt5.QtCore import QObject, pyqtSignal, Qt

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from encoding_pkg.encoding_help_dialog_ui import Ui_Dialog as help_dialog 
from encoding_pkg.plotwidgets import categoricalplot_widget, noncategoricalplot_widget
from encoding_pkg import encoder

class encoding_view(QObject):

    unknown_error = pyqtSignal(str, str)
    success_dialog_signal = pyqtSignal(str)
    update_runtime = pyqtSignal(str)

    """
    Initialize the encoding widget
        - Hide expert discretization settings
        - Connect buttons to signals
    """
    def __init__(self, ui):
        super(encoding_view, self).__init__()
        self.ui_ = ui
        self.ui_.expertsettings.hide()
        self.ui_.noncategoricalsettings.layout().setContentsMargins(9, 15, 9, 9)
        self.ui_.noncategoricalsettings.layout().setSpacing(6)
        self.ui_.datatype.currentIndexChanged.connect(self.handle_datatype_changed)
        self.ui_.discretizations.currentIndexChanged.connect(self.handle_discretizations_changed)
        self.ui_.browsebutton.clicked.connect(self.handle_browsebutton_clicked)
        self.ui_.startbutton.clicked.connect(self.handle_startbutton_clicked)
        self.update_runtime.connect(self.ui_.runtime.setText)
        self.success_dialog_signal.connect(self.show_success_dialog)
        self.unknown_error.connect(self.showerrormessage)

    """
    Opens up a help dialog in a new window
    """
    def handle_helpbutton_clicked(self):
        self.help_window = help_dialog()
        self.help_window.show()

    """
    Opens up the non-categorical settings box if 'Non-Categorical' is selected
    Hides it if it's not selected
    """
    def handle_datatype_changed(self):
        if self.ui_.datatype.currentText() == "Non-Categorical":
            self.ui_.noncategoricalsettings.show()
            self.ui_.noncategoricalsettings.layout().setContentsMargins(9, 15, 9, 9)
            self.ui_.noncategoricalsettings.layout().setSpacing(6)
        else:
            self.ui_.noncategoricalsettings.hide()

    """
    Opens up the expert dicretizations box if 'Expert' discretizations are selected
    Hides it if it's not selected
    """
    def handle_discretizations_changed(self):
        if self.ui_.discretizations.currentText() == "Expert":
            self.ui_.expertsettings.show()
            self.ui_.expertsettings.layout().setContentsMargins(9, 9, 9, 9)
            self.ui_.expertsettings.layout().setSpacing(6)
        else:
            self.ui_.expertsettings.hide()

    """
    Opens up a file explorer to select an input file
    The inputfile path is copied to the inputfile line edit box
    """
    def handle_browsebutton_clicked(self):
        filepath = QtWidgets.QFileDialog.getOpenFileName()
        self.ui_.inputfile.setText(filepath[0])

    """
    Attempts to encode the file in the inputfile line edit box
 	Will throw errors if there are any
    """
    def handle_startbutton_clicked(self):
        if self.validateinputfile():
            self.ui_.startbutton.setEnabled(False)
            try:
                legend = self.generatelegend()
                if self.ui_.datatype.currentText() == 'Non-Categorical' and \
                    self.ui_.discretizations.currentText() != 'Expert':
                    if len(legend.keys()) != int(self.ui_.discretizations.currentText()):
                        self.showwarningmessage('Warning', 'Only ' + str(len(legend.keys())) + ' discretizations possible')
            except ValueError as err:
                self.showerrormessage("Bound Error", str(err))
                self.ui_.startbutton.setEnabled(True)
                return
            else:
                self.plotfrequencies(legend)
            t = threading.Thread(target=self.startprogram)
            t.start()

    """
    The file should be valid at this point
    So encoding now takes place and fills the output tables with the results
    Also shows a success dialog at the end assuming no error has been caught
    """
    def startprogram(self):
        output_filename = ""
        try:
            start_time = timeit.default_timer()
            legend = self.generatelegend()
            table = self.ui_.results_table
            while table.rowCount():
                table.removeRow(0)
            results = self.generateresults(legend)
            end_time = timeit.default_timer()
            ms = (end_time-start_time) * 1000.0 % 1000
            m, s = divmod(end_time-start_time, 60)
            h, m = divmod(m, 60)
            string_runtime =  "%dh %02dm %02ds %03dms " % (h, m, s, ms)
            self.outputresults(results, string_runtime)
            self.outputlegend(legend)
            #
            self.update_runtime_log(string_runtime)
            output_filename = self.createcsv(results, legend, string_runtime)
            self.success_dialog_signal.emit("Output file at " + '\n' + 
                "/encoding_pkg/logs/" + output_filename)
        except Exception as e:
            self.unknown_error.emit("Unknown Exception", str(e))
        self.ui_.startbutton.setEnabled(True)

    """
    Validates the file as being proper input
    Decides to validate it as a categorical or non-categorical file 
    """
    def validateinputfile(self):
        if self.ui_.datatype.currentText() == "Categorical":
            return self.validate_categoricalcsv()
        elif self.ui_.datatype.currentText() == "Non-Categorical":
            return self.validate_noncategoricalcsv()

    """
    Returns true if:
    	The file exists
    	The there are at max 5 different strings
    Else returns false
    """
    def validate_categoricalcsv(self):
        inputfile = ""
        try:
            inputfile = open(self.ui_.inputfile.text(), 'r').read().splitlines()
        except FileNotFoundError:
            self.showerrormessage("File Error", "File not found")
            return False
        no_gwf_data = []
        for line in inputfile:
            no_gwf_data.append(line.replace(',',' ').split()[:-1])
        categories = set()
        for line in no_gwf_data:
            for item in line:
                categories.add(item.strip(' ').lower())
        if len(categories) > 5:
            self.showerrormessage("File Error", 
                "Too many categories in input file, must be less than 5\n" + str(categories))
            return False
        return True

    """
    Returns true if:
    	The file exists
    	Any value is not numerical
    Else returns false
    """
    def validate_noncategoricalcsv(self):
        inputfile = ""
        try:
            inputfile = open(self.ui_.inputfile.text(), 'r').read().splitlines()
        except FileNotFoundError:
            self.showerrormessage("File Error", "File not found")
            return False
        no_gwf_data = []
        for line in inputfile:
            no_gwf_data.append(line.replace(',',' ').split()[:-1])
        for line in no_gwf_data:
            for item in line:
                if item != '':
                    try:
                        float(item)
                    except ValueError:
                        self.showerrormessage("File Error", 
                            "Found invalid cell in input file : " + str(item))                        
                        return False
        return True

    """
    Creates the legend that is necessary by the inputs 
    If Categorical is selected, finds a categorical legend
    If non-categorical and not expert is selected
    	uses the findboundaries function in encoder to find the legend
    If expert settings are selected
    	Uses the expert settings to create a legend
    	Throws an error if the legend in the input settings are not valid
    """
    def generatelegend(self):
        if self.ui_.datatype.currentText() == "Categorical":
            return encoder.findcategories(self.ui_.inputfile.text())
        elif self.ui_.datatype.currentText() == "Non-Categorical":
            if self.ui_.discretizations.currentText() != "Expert":
                return encoder.findboundaries(self.ui_.inputfile.text(), 
                    int(self.ui_.discretizations.currentText()))
            else:
                legend = dict()
                if self.ui_.a_upperbound.value() or self.ui_.a_lowerbound.value():
                    legend['A'] = (int(self.ui_.a_lowerbound.value()), int(self.ui_.a_upperbound.value())) 
                if self.ui_.b_upperbound.value() or self.ui_.b_lowerbound.value():
                    legend['B'] = (int(self.ui_.b_lowerbound.value()), int(self.ui_.b_upperbound.value())) 
                if self.ui_.c_upperbound.value() or self.ui_.c_lowerbound.value():
                    legend['C'] = (int(self.ui_.c_lowerbound.value()), int(self.ui_.c_upperbound.value())) 
                if self.ui_.d_upperbound.value() or self.ui_.d_lowerbound.value():
                    legend['D'] = (int(self.ui_.d_lowerbound.value()), int(self.ui_.d_upperbound.value())) 
                if self.ui_.e_upperbound.value() or self.ui_.e_lowerbound.value():
                    legend['E'] = (int(self.ui_.e_lowerbound.value()), int(self.ui_.e_upperbound.value())) 

                try: 
                    self.validate_expertlegend(legend)
                except ValueError as err:
                    raise err
                else:
                    return legend

    """
    Checks if the legend created by expert discretizations is valid
    Throws an error if:
    	No bounds are specified
    	Any lower bound is larger than the upper bound
    	Any bounds are overlapping
    	The bounds are not inclusive of all the input in the input file
    """
    def validate_expertlegend(self, legend):
        if not legend:
            raise ValueError("No bounds specified")
        for event in legend:
            if legend[event][0] > legend[event][1]:
                raise ValueError("Lower bound larger than upperbound")
        ranges = []  
        for event in legend:
            ranges += range(legend[event][0], legend[event][1])
        if len(ranges) != len(set(ranges)): #check for overlapping ranges
            raise ValueError("Bounds Overlapping")
        data = [] 
        with open(self.ui_.inputfile.text(), 'r') as f:
            for row in f.read().splitlines():
                data += row.replace(',', ' ').split()[:-1]
        for datum in data:
            fit = False
            for event in legend:
                if float(datum) >= legend[event][0] and float(datum) < legend[event][1]:
                    fit = True
                    break
            if not fit:
                raise ValueError("Bounds not inclusive of all input") 

    """
    Decides whether to create categorical or non-categorical results
    """
    def generateresults(self, legend):
        if self.ui_.datatype.currentText() == "Categorical":
            return encoder.encode_categoricalfile(self.ui_.inputfile.text(), legend)
        elif self.ui_.datatype.currentText() == "Non-Categorical":
            return encoder.encode_datafile(self.ui_.inputfile.text(), legend)

    """
    Using the input file and the legend
    Creates a frequency plot of the data in the file 
    Sets self.ui_.frequenciescontent as parent to attach it 
    """
    def plotfrequencies(self, legend):
        layout = self.ui_.frequenciescontent.layout()
        filename = self.ui_.inputfile.text()
        plot = None
        if self.ui_.datatype.currentText() == 'Categorical':
            plot = categoricalplot_widget(filename, self.ui_.frequenciescontent)
        elif self.ui_.datatype.currentText() == 'Non-Categorical':
            plot = noncategoricalplot_widget(filename, self.ui_.frequenciescontent)
        if layout.count():
            layout.itemAt(0).widget().setParent(None)
        self.ui_.frequenciescontent.setContentsMargins(1,1,1,1)
        layout.addWidget(plot)

    """
    ARGS runtime - a string representing the runtime of the program

    Writes to a new line in encoding_runtimes.csv that exists in the same directory
    The new line is in the format [current_datetime, input_filepath, runtime]  
    """
    def update_runtime_log(self, runtime):
        output_filepath = os.path.dirname(os.path.realpath(__file__)) + '\\encoding_runtimes.csv'
        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
        input_filepath = self.ui_.inputfile.text() 
        with open(output_filepath, 'a') as runtime_file:
            runtime_file.write(current_datetime + ',' + 
                               input_filepath + ' ,' + 
                               runtime + '\n')

    """
    ARGS filename -  a string of the path to the input file
    RETURNS a list of the gwfs in the input file
    """
    def get_gwfs(self, filename):
        expressionfile = open(filename, 'r').read().splitlines()
        gwf_data = []
        for line in expressionfile:
            gwf_data.append(line.replace(',',' ').split()[-1])
        return gwf_data
    """
    ARGS 
        results - 
        legend - 
        runtime - 
    """
    def createcsv(self, results, legend, runtime):
        gwf_data = self.get_gwfs(self.ui_.inputfile.text())
        results_with_gwf = []
        for i in range(len(results)):
            results_with_gwf.append((results[i], gwf_data[i]))
        try:
            return encoder.createcsv(results_with_gwf, self.ui_.inputfile.text())
        except PermissionError:
            self.showerrormessage("File Error", "Output file permission error, already open?")

    """
    ARGS
        results - a list of tuples, A list of tuples, each tuple being (encoded string, gwf)
                The gwf must be in string format
        runtime - a string of the runtime of the program
    """
    def outputresults(self, results, runtime):
        row = 0
        table = self.ui_.results_table
        for result in results:
            table.insertRow(row)
            table.setItem(row, 0, QtWidgets.QTableWidgetItem(result))
            row += 1
        self.update_runtime.emit(str(runtime))


    """
    From a dictionary which has keys A-E or less
    This functions writes to the UI displaying the legend
    """
    def outputlegend(self, legend):
        if 'A' in legend:
            if type(legend['A']) == list:
                self.ui_.a_legendbounds.setText('['+str(legend['A'][0])+','+str(legend['A'][1])+')')
            else:
                self.ui_.a_legendbounds.setText(str(legend['A']))
        else:
            self.ui_.a_legendbounds.setText("n/a")
        if 'B' in legend:
            if type(legend['B']) == list:
                self.ui_.b_legendbounds.setText('['+str(legend['B'][0])+','+str(legend['B'][1])+')')
            else:
                self.ui_.b_legendbounds.setText(str(legend['B']))
        else:
            self.ui_.b_legendbounds.setText("n/a")
        if 'C' in legend:
            if type(legend['C']) == list:
                self.ui_.c_legendbounds.setText('['+str(legend['C'][0])+','+str(legend['C'][1])+')')
            else:
                self.ui_.c_legendbounds.setText(str(legend['C']))
        else:
            self.ui_.c_legendbounds.setText("n/a")
        if 'D' in legend:
            if type(legend['D']) == list:
                self.ui_.d_legendbounds.setText('['+str(legend['D'][0])+','+str(legend['D'][1])+')')
            else:
                self.ui_.d_legendbounds.setText(str(legend['D']))
        else:
            self.ui_.d_legendbounds.setText("n/a")
        if 'E' in legend:
            if type(legend['E']) == list:
                self.ui_.e_legendbounds.setText('['+str(legend['E'][0])+','+str(legend['E'][1])+')')
            else:
                self.ui_.e_legendbounds.setText(str(legend['E']))
        else:
            self.ui_.e_legendbounds.setText("n/a")

    def showerrormessage(self, title, message):
        error_message = QtWidgets.QMessageBox.critical(self.ui_, title, message)

    def showwarningmessage(self, title, message):
        error_message = QtWidgets.QMessageBox.warning(self.ui_, title, message)

    def show_success_dialog(self, message):
        success_dialog = QtWidgets.QMessageBox.information(self.ui_, "Categorization Successful", message)
