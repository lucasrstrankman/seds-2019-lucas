#!/usr/bin/env
import csv
from collections import Counter

from PyQt5 import QtWidgets
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib import pyplot
import numpy as np
        
class categoricalplot_widget(FigureCanvas):
    def __init__(self, filename, parent=None, width=10, height=10, dpi=60):
        fig = Figure(figsize=(width,height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        self.axes.hold(False)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        data = []
        datafile = open(filename, 'r').read().splitlines()
        no_gwf_data = []
        for line in datafile:
            no_gwf_data.append(line.replace(',',' ').split()[:-1])
        for row in no_gwf_data:
            for item in row:
                if item is not '':
                    try: 
                        data.append(item.strip(' ').lower())
                    except AttributeError:
                        data.append(item)
        counts = Counter(data)
        self.axes.bar(range(len(counts)), counts.values(), align='center')
        names = counts.keys()
        self.axes.set_ylabel('Frequency of Occurence')
        self.axes.set_xlabel('Categories')
        self.axes.set_xticks(np.arange(len(names)))
        self.axes.set_xticklabels(list(names))
        self.axes.set_ylim(0, self.axes.get_ylim()[1] * 1.1)

        FigureCanvas.setSizePolicy(self,
                QtWidgets.QSizePolicy.Preferred,
                QtWidgets.QSizePolicy.Preferred)
        FigureCanvas.updateGeometry(self)


class noncategoricalplot_widget(FigureCanvas):
    def __init__(self, filename, parent=None, width=10, height=10, dpi=60):
        fig = Figure(figsize=(width,height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        self.axes.hold(False)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        data = []
        datafile = open(filename)
        no_gwf_data = []
        for line in datafile:
            no_gwf_data.append(line.replace(',',' ').split()[:-1])
        for row in no_gwf_data:
            for item in row:
                if item is not '':
                    try: 
                        data.append(item.strip(' ').lower())
                    except AttributeError:
                        data.append(item)
        rounded_data = []
        for datum in data:
            try:
                int(datum)
            except ValueError:
                rounded_data.append(round(float(datum)))
            else:
                rounded_data.append(datum)
        data = np.array(rounded_data)
        data = data.astype(int)
        self.axes.hist(data, bins=range(data.min(), data.max()+1))
        self.axes.set_ylabel('Frequency of Occurence')
        self.axes.set_xlabel('Numerical Data')
        x_range = self.axes.get_xlim()[1] - self.axes.get_xlim()[0]
        new_lower_xlim = self.axes.get_xlim()[0] - x_range * 0.05
        new_upper_xlim = self.axes.get_xlim()[1] + x_range * 0.05
        self.axes.set_xlim(new_lower_xlim, new_upper_xlim)
        self.axes.set_ylim(0, self.axes.get_ylim()[1] * 1.1)
        FigureCanvas.setSizePolicy(self,
                QtWidgets.QSizePolicy.Preferred,
                QtWidgets.QSizePolicy.Preferred)
        FigureCanvas.updateGeometry(self)
