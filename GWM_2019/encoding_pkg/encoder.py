#!/usr/bin/env
import csv
import datetime
import os
import sys

import numpy
import matplotlib.pyplot as plt

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


"""
Finds the boundaries to evenly distribute numerical data in a file

ARGS
	-filepathname
		A string of a file path 
		containing rows of numerical data comma separated
		With a numerical GWF at the end of each line space separated
	-cyclecount
		The number of discretizations, will be the amount of boundaries
		to break up the data into

RETURNS
	A dict that is up to the size of cyclecount
	The keys starting from A, incrementing up to E
	The values being the boundaries for that letter in a tuple
	The lower bound being inclusive
	The upper bound being exclusive
"""
def findboundaries(filepathname, cyclecount):
    # read in a csv file of numerical data
    # observations = numpy.genfromtxt(filepathname, delimiter=',')
    observations = []
    csvfile = get_data_only(filepathname)
    for line in csvfile:
        for item in line:
            if item is not '':
                observations.append(float(item))

    observations = numpy.array(observations)
    observations = observations.astype(int)
    # find boundaries among the data
    boundaries = []
    for i in range(1, cyclecount+1):
        boundaries.append(round(numpy.percentile(observations,
                                                 i * 100/cyclecount)))

    startingletter = 'A'
    minvalue = numpy.amin(observations)

    result = {}    # output = ''
    for i in range(0, cyclecount):
        if i is 0:
            result[chr(ord(startingletter) + i)] = \
                [numpy.int16(minvalue), numpy.int16(boundaries[i])]
        elif i is cyclecount-1:
            result[chr(ord(startingletter) + i)] = \
                [numpy.int16(boundaries[i-1]), numpy.int16(boundaries[i])+1]
        else:
            result[chr(ord(startingletter) + i)] = \
                [numpy.int16(boundaries[i-1]), numpy.int16(boundaries[i])]

    return result

"""
ARGS
	-filepathname
		A string of a file path
		Of a series of strings comma separated
		With a numerical GWF at the end of each line space separated

RETURNS
	The different strings found in the file path
	Not case sensitive
"""
def findcategories(filepathname):
    csvfile = get_data_only(filepathname)
    observations = []
    for row in csvfile:
        observations += [item.strip(' ').lower() for item in row]
    categories = numpy.unique(observations)
    result = {}
    startingletter = 'A'
    i = 0
    while i < len(categories):
        result[chr(ord(startingletter) + i)] = categories[i]
        i += 1
    return result

"""
ARGS
	-legend
		A dict where each key is a string
		And each value tuple of 2 containig numerical bounds
		The lower bound being inclusive and the upper bound being exclusive
	-number
		The number to be replaced by a letter
RETURNS
	The letter corresponding to where the number fits in the legend

Throws an error if the number can't be matched to a key in the legend 
"""
def replace_number(legend, number):
    for key in legend:
        bounds = legend[key]
        #string to float to int
        if int(float(number)) >= bounds[0] and int(float(number)) < bounds[1]:
            return key
    raise ValueError("replacenumber error")

"""
RETURNS 
	a list of each row in filename
	after removing the gwf at the end of each line
"""
def get_data_only(filename):
    expressionfile = open(filename, 'r').read().splitlines()
    no_gwf_data = []
    for line in expressionfile:
        no_gwf_data.append(line.replace(',',' ').split()[:-1])
    return no_gwf_data

"""
ARGS
	-filename
		A string of a filepath that is comma separated strings
	-legend
		A dict where each key is a letter, and the value is 
		a category corresponding to that letter, not case sensitive

RETURNS
	A list of strings where each letter in the string is 
	representative of the comma separated string that was in the file
"""
def encode_categoricalfile(filename, legend):
    expressionfile = get_data_only(filename)
    expressions = []
    observations = []
    for row in expressionfile:
        observations.append(row)
    for observation in observations:
        index = 0
        result = ''
        while index < len(observation):
            for key in legend:
                if observation[index].strip(' ').lower() == legend[key]:
                    result += key
            index += 1
        expressions.append(result)
    return expressions

"""
ARGS
	-filename
		A string of a filepath that is comma separated strings
	-legend
		A dict where each key is a letter
		And each value is a tuple of 2 representing boundaries
		that the letter will represent

RETURNS
	A list of strings where each letter in the string is 
	representative of the numerical value that was in the file
	As dictated by the legend
"""
def encode_datafile(filename, legend):
    expressionfile = get_data_only(filename) 
    expressions = []
    for line in expressionfile:
        encodedline = ''
        for number in line:
            encodedline += replace_number(legend, str(number))
        expressions.append(encodedline)
    return expressions

"""
ARGS
    - results - A list of tuples, each tuple being (encoded string, gwf)
                The gwf must be in string format
    - inputpath - a string of the original input, used to determine outputfile name

RETURNS the determine outputfile name
"""
def createcsv(results, inputpath):
    save_path = os.path.dirname(os.path.realpath(__file__)) + '\\logs\\'
    inputfile = inputpath[inputpath.rfind('/')+1:inputpath.rfind('.')]
    output_filename = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S_" + inputfile + ' output_log.csv')
    outputfile = os.path.join(save_path, output_filename)
    inputfile_data = get_data_only(inputpath)
    inputfile_strings = []
    for row in inputfile_data:
        inputfile_strings.append(','.join(row).replace(',','; '))
    results_with_original_input = []
    for i in range(len(results)):
        results_with_original_input.append((inputfile_strings[i], 
                                            results[i][0],
                                            results[i][1]))

    with open(outputfile, "w") as outputfile:
        for result in results_with_original_input:
            outputfile.write(result[0] + ',' + result[1] + ',' + result[2] + "\n")

    return output_filename


def main():
    inputfile = "discretization_sample_data.csv"
    outputfile = "encodingtest.csv"

    datacycles = int(input("How many datacycles? 2-4\n"))
    legend = findboundaries(inputfile, datacycles)
    expressions = encode_datafile(inputfile, legend)
    results = findregexes(expressions)
    print("Expressions are : ")
    for expression in expressions:
        print("     " + expression)
    print("Creating csv")
    createcsv(legend, results, outputfile)
    print("Finished")

if __name__ == '__main__':
    main()
