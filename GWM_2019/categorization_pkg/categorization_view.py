#!/usr/bin/env
import datetime
import os
import sys
import threading
import timeit

from PyQt5 import QtWidgets
from PyQt5.QtCore import QObject, pyqtSignal

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from categorization_pkg.categorization_help_dialog_ui import Ui_Dialog as help_dialog 
from categorization_pkg.categorizer import categorizer
from synthesizing_pkg.node_selector import node_selector

class categorization_view(QObject):

    unknown_error = pyqtSignal(str, str)
    regex_dict_results = pyqtSignal(dict, str)
    success_dialog_signal = pyqtSignal(str)
    update_categorization_runtime = pyqtSignal(str)
    update_node_selection_runtime = pyqtSignal(str)

    """
    Initialize the categorization widget
        - Intiailizes categorizer
        - Connect signal to slots
    """    
    def __init__(self, ui):
        super(categorization_view, self).__init__()
        self.ui_ = ui
        self.categorizer_ = categorizer()
        self.ui_.inputfile_explorerbutton.clicked.connect(self.handle_inputfile_explorerbutton_clicked)
        self.ui_.startbutton.clicked.connect(self.handle_startbutton_clicked)
        self.unknown_error.connect(self.showerrormessage)
        self.success_dialog_signal.connect(self.show_success_dialog)
        self.update_categorization_runtime.connect(self.ui_.categorization_runtime.setText)
        self.update_node_selection_runtime.connect(self.ui_.node_selection_runtime.setText)

    """
    Opens up a help dialog in a new window
    """
    def handle_helpbutton_clicked(self):
        self.help_window = help_dialog()
        self.help_window.show()

    """
    Opens up a file explorer to select an input file
    The inputfile path is copied to the inputfile line edit box
    """
    def handle_inputfile_explorerbutton_clicked(self):
        filepath = QtWidgets.QFileDialog.getOpenFileName()
        self.ui_.input_filepath_label.setText(filepath[0])

    """
    Handles clicking the start button
    Validates the file. 
    If the file is good, it clears the ui
    Otherwise shows an error message
    """
    def handle_startbutton_clicked(self):
        filepath = self.ui_.input_filepath_label.text()
        try:
            self.validate_input_file(filepath)
        except ValueError as err:
            self.showerrormessage("File Error", str(err))
        else:
            self.ui_.startbutton.setEnabled(False)
            while self.ui_.categorized_strings_table.rowCount():
                self.ui_.categorized_strings_table.removeRow(0)
            while self.ui_.artificial_nodes_table.rowCount():
                self.ui_.artificial_nodes_table.removeRow(0)
            self.ui_.node_selection_runtime.setText('')
            self.ui_.categorization_runtime.setText('')
            t = threading.Thread(target=self.startprogram)
            t.start()

    """
    Checks that the input file has 2 columns for each line
    Checks that each given expression only has 5 events
    Checks that the second column of every row is a float
    """
    def validate_input_file(self, filepath):
        regexfile = []
        linenumber = 0
        try:
            regexfile = open(filepath, 'r').read().splitlines()
        except FileNotFoundError:
            self.showerrormessage("File Error", "File not found")
            return False
        for line in regexfile:
            linenumber += 1
            line = line.split()
            if len(line) != 2:
                raise ValueError("Line " + str(linenumber) + " more than 2 column")
                return False
            elif len(set(line[0])) > 5:
                raise ValueError("Line " + str(linenumber) + " expression more than 5 events")
                return False
            elif not self.isFloat(line[1]):
                raise ValueError("Line " + str(linenumber) + " GWF not float")
                return False
        return True

    """
    RETURNS true if number is float
    else RETURNS false
    """
    def isFloat(self, number):
        try:
            float(number)
        except ValueError:
            return False
        else:
            return True

    """
    Runs the program. Has a try statement to catch any errors that haven't been thought of or coded against
    enabling the start button is outside the try block so that the program will never crash and close/freeze
    """
    def startprogram(self):
        output_filename = ''
        try:
            inputfile = self.ui_.input_filepath_label.text()
            start_time = timeit.default_timer()
            categorized_strings = self.categorize_strings(self.ui_.input_filepath_label.text())
            end_time = timeit.default_timer()
            #
            ms = (end_time-start_time) * 1000.0 % 1000
            m, s = divmod(end_time-start_time, 60)
            h, m = divmod(m, 60)
            categorization_runtime_string =  "%dh %02dm %02ds %03dms " % (h, m, s, ms)
            #
            start_time = timeit.default_timer()
            regexes = []
            for result in categorized_strings:
                regexes += list(result[1].keys())
            #
            translated_regexes, legend = self.translate_regexes(regexes)
            selector = node_selector(translated_regexes)
            translated_artificial_regexes = selector.artificial_nodes
            untranslated_artificial_regexes = self.untranslate_regexes(translated_artificial_regexes, legend)
            #
            end_time = timeit.default_timer()
            ms = (end_time-start_time) * 1000.0 % 1000
            m, s = divmod(end_time-start_time, 60)
            h, m = divmod(m, 60)
            node_selection_runtime_string =  "%dh %02dm %02ds %03dms " % (h, m, s, ms)
            #
            self.update_categorization_runtime.emit(categorization_runtime_string)
            self.update_node_selection_runtime.emit(node_selection_runtime_string)
            self.populate_artificial_nodes_table(untranslated_artificial_regexes)
            self.populate_categorized_strings_table(categorized_strings)
            self.regex_dict_results.emit(self.build_regex_dict(categorized_strings), inputfile)
            # print(str(self.build_regex_dict(categorized_strings)))
            #
            output_filename = self.create_outputfile(inputfile, categorized_strings)
            self.update_runtime_log(categorization_runtime_string, inputfile, "categorization_runtimes.csv")
            self.update_runtime_log(node_selection_runtime_string, inputfile, "Selecting Minimal Sub Hierarchy_runtimes.csv")
            self.success_dialog_signal.emit("Output file at " + '\n' + 
                "/encoding_pkg/logs/" + output_filename)
        except Exception as e:
            self.unknown_error.emit("Unknown Exception", str(e))
        self.ui_.startbutton.setEnabled(True)

    """
    ARGS categorized_strings is a list of tuples. 
         The first item in the tuple is the original uncategorized string
         The second item is a dictionary where each key is a regex that the string
            has been categorized into, and the values are the distances from the root
         The third item is the GWF associated with that string
    RETURNS a dict where each key is a regular expression
            and the values are a list of GWFs associated with that regex
    """
    def build_regex_dict(self, categorized_strings):
        regex_dict = dict()
        for string_group in categorized_strings:
            regex = max(string_group[1].keys())
            if regex not in regex_dict:
                regex_dict[regex] = []
            regex_dict[regex].append(float(string_group[2]))
        return regex_dict

    """
    ARGS untranslated_regexes - Takes in a list of regexes consisting of any letters
    RETURNS translated_regexes - A list of the original regexes but with literals 
                                 mapping to A,B,C,D, or E
            legend - A dict where each key is A,B,C,D, or E
                     each value is the letter being represented by the key
    """
    def translate_regexes(self, untranslated_regexes):
        translated_regexes = []
        legend = dict()
        current_letter = 'A'
        
        untranslated_letters = set(''.join(untranslated_regexes))
        untranslated_letters.discard('*')
        untranslated_letters.discard('(')
        untranslated_letters.discard(')')

        for letter in untranslated_letters:
            legend[current_letter] = letter
            current_letter = chr(ord(current_letter) + 1)

        inv_legend = {v:k for k,v in legend.items()}
        for regex in untranslated_regexes:
            translated_regex = ""
            for char in regex:
                if char in inv_legend:
                    translated_regex += inv_legend[char]
                else:
                    translated_regex += char
            translated_regexes.append(translated_regex)

        return translated_regexes, legend  

    """
    Does the opposite of translate_regexes
    """
    def untranslate_regexes(self, translated_regexes, legend):
        untranslated_regexes = []
        for translated_regex in translated_regexes:
            untranslated_regex = ''
            for char in translated_regex:
                if char in legend:
                    untranslated_regex += legend[char]
                else:
                    untranslated_regex += char
            untranslated_regexes.append(untranslated_regex)
        return untranslated_regexes

    """
    regexes - A list of strings
    Populates the artifical nodes table where each row is an item in regexes
    """
    def populate_artificial_nodes_table(self, regexes):
        row = 0
        table = self.ui_.artificial_nodes_table
        table.insertRow(row)
        for regex in regexes:
            table.setItem(row, 0, QtWidgets.QTableWidgetItem(regex))
            row += 1
            table.insertRow(row)
        table.removeRow(table.rowCount()-1)

    """
    ARGS - filepath - A string of the inputfile
    RETURNS - A list of tuples, where each tuple is 
              [0] The original string to be categorized
              [1] A dictionary of regexes that hte original string has been categorized into
              [2] A string representing the GWF associated with that string
    """
    def categorize_strings(self, filepath):
        encoded_string_file = []
        results = []
        encoded_string_file = open(filepath, 'r').read().splitlines()
        for line in encoded_string_file:
            expression = line.split()[0]
            gwf = line.split()[1]
            regexes = self.categorizer_.findregex(expression.strip())
            results.append((expression, regexes, gwf))
        return results


    """
    categorized_strings in format [(string, {regex: distance})]
    """
    def populate_categorized_strings_table(self, categorized_strings):
        row = 0
        table = self.ui_.categorized_strings_table
        table.insertRow(row)
        for result in categorized_strings:
            table.setItem(row, 0, QtWidgets.QTableWidgetItem(result[0]))
            for regex in result[1]:
                table.setItem(row, 1, QtWidgets.QTableWidgetItem(regex))
                table.setItem(row, 2, QtWidgets.QTableWidgetItem(result[1][regex]))
                row += 1
                table.insertRow(row)
        table.removeRow(table.rowCount() - 1)

    """
    ARGS 
        inputpath - A string of the outputfile path
        results - a list of tuples where
                    [0] original expression
                    [1] a list of regexes
                    [2] GWF as a string
    Writes an output file in the format of 
        original_expression categorized_regexes gwf
        All space separated
    each entry being line separated
    
    RETURNS - The output file name
    """
    def create_outputfile(self, inputpath, results):
        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S ")
        output_filename = current_datetime + inputpath[inputpath.rfind('/')+1:inputpath.rfind('.')] + ' output_log.csv'
        output_filepath = os.path.dirname(os.path.realpath(__file__)) + '\\logs\\' + output_filename
        with open(output_filepath, 'w') as outputfile:
            for result in results:
               outputfile.write(result[0] + '   ')
               for regex in result[1]:
                   outputfile.write(regex + '   ')
               outputfile.write(result[2])
               outputfile.write('\n')
        return output_filename


    """
    Updates the runtime log file
    ARGS
        runtime - the runtime of the program in string format
        input_filepath - the inputpath of the input file
        output_filepath - The name of the output file
    """
    def update_runtime_log(self, runtime, input_filepath, outputfile):
        output_filepath = os.path.dirname(os.path.realpath(__file__)) + '\\' + outputfile
        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
        with open(output_filepath, 'a') as runtime_file:
            runtime_file.write(current_datetime + ',' +
                               input_filepath + ' ,' + 
                               runtime + '\n')

    def showerrormessage(self, title, message):
        error_message = QtWidgets.QMessageBox.critical(self.ui_, title, message)

    def show_success_dialog(self, message):
        success_dialog = QtWidgets.QMessageBox.information(self.ui_, "Categorization Successful", message)
