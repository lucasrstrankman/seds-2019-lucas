import itertools
import os
import re
import sys

"""
The only function of interest in this module is findregex()
All other functions are supporting functions
"""
PATH = os.path.dirname(os.path.abspath(__file__))


"""
This class categorizes an encoded string into a regex.
The class uses a regex template directed graph. Directed 
meaning that there are children and parents. This class 
organizes the regexes by level, a regexes level defined as 
the regex's shortest distance from the root. It attempts to match
the string to each regex level by level starting from the bottom
most level and takes the shortest regexes of the results.  

One thing that comes with being able to use a regex in python
i.e. matching a string to it is that it must be compiled into a 
regex object. Compiling a regex is time consuming, and compiling 
all the regexes upon instantiation of this class is also time-consuming.
So a caching method is used where everytime a regex is compiled, it will
be stored in self.compiled_regexes. But regexes will only be compiled 
when required.
"""
class categorizer():

    def __init__(self):
        self.level_graphs_ = []
        for i in range(5):
            self.level_graphs_.append(self.create_level_graphfromfile('bottomupresults' + str(i+1) + '.txt'))
        self.compiled_regexes = dict()

    """
    Return a dict where the keys are the levels as strings
    and the values are the regexes in each level
    """
    def create_level_graphfromfile(self, filename):
        filename = PATH + "/" + filename
        try:
            regexfile = open(filename, 'r').read().splitlines()[:-1]
        except FileNotFoundError:
            raise FileNotFoundError("Missing " + filename)
    
        level_graph = dict()
        
        for line in regexfile:
            if 'children' not in line and 'parents' not in line:
                regex = line.strip().split()[0]
                level = line.strip().split()[1]
                if level not in level_graph:
                    level_graph[level] = [regex]
                else:
                    level_graph[level].append(regex)
        return level_graph

    """
    This function is more a request to compile a regex
    This function checks if a regex is compiled in self.compiled_regexes
    If it has already been compiled, it will return and do nothing
    If the regex has not been compiled, it first checks if the regex has 
    the maximum amount of stars such as (A*B*)* or (C*E*D*)*. If it has
    the maximum amount of stars it will compile a simplified regex, 
    so by example (A*B*)* will become [A|B]*, (C*E*D*)* will become [C|E|D]*
    Otherwise, it will compile the regex as is. 

    The conversion of the regex is so that the regex object will be faster to 
    work with. Attempting to find a match with [C|E|D]* is significantly faster
    than attempting to find a match with (C*E*D*)* even though they are equivalent
    regexes.
    """
    def compile_regex(self, regex):
        if regex in self.compiled_regexes:
            # print('cache hit!') # debug
            return
        else:
            if regex.count('*')-1 == len(regex.replace('*', '').replace('(', '').replace(')','')):
                optimized_regex = regex.replace('(','[').replace('*', '|').replace('|)|', ']*')
                self.compiled_regexes[regex] = re.compile('^' + optimized_regex + '$')
                # print('optimized ' + regex) # debug
            else:
                self.compiled_regexes[regex] = re.compile('^' + regex + '$')
 
    """
    Creates a key for expression to be encoded using the five letters
    ABCDE, this function trusts that the given expression only has
    at most 5 unique letters.
    The return value is a dict in the form 
    {'S':'A', 'M':'B','L':'C'} 
    where the given expression contains S,M,L (ex. SMSLLLSM)
    """
    def expression_encode_dict(self, expression):
        encodekey = {}
        for char in expression:
            if char not in encodekey:
                encodekey[char] = str(chr(ord('A') + len(encodekey)))
        return encodekey


    """
    Using the given dict, translates an expression into an expression
    containing the values in the dict
    ex. encoding_dict = {'S':'A','M':'B'}
    expression = 'SSSMSM'
    returns 'AAABAB'
    """
    def translate_expression(self, encoding_dict, expression):
        encoded_expression = ""
        for char in expression:
            encoded_expression += encoding_dict[char]
        return encoded_expression


    """
    Does the opposite of translate_expression
    ex. encoding_dict = {'S':'A','M':'B'}
    expression = 'AAABAB'
    returns 'SSSMSM'
    """
    def translate_encoded_expression(self, encoding_dict, encoded_expression):
        inv_encoding_dict = {v: k for k, v in encoding_dict.items()}
        expression = ""
        for char in encoded_expression:
            if char in inv_encoding_dict:
                expression += inv_encoding_dict[char]
            else:
                expression += char
        return expression

    """
    Checks to see if the regex can completely match the given expression
    Ex. (AB)* and ABAB returns True
        A*B*C* and AABBCA returns False
    """
    def completematch(self, regex, expression):
        self.compile_regex(regex)
        if self.compiled_regexes[regex].match(expression):
            return True
        else:
            return False

    """
    Returns the first letter in the regex
    Handles the case of something like (A*B*)* where the first
    letter would be 'A', not '('
    """
    def first_letter_in_regex(self, regex):
        if regex[0] == '(':
            return regex[1]
        else:
            return regex[0]


    """
    Removes all the regexes in the graph where the first_letter of the
    regex does not match the first_letter given 
    """
    def remove_not_matching_first_letter_nodes(self, level_graph, first_letter):
        unrelated_regexes = dict()
        for level in level_graph.copy():
            unrelated_regexes[level] = []
            for regex in level_graph.copy()[level]:
                if self.first_letter_in_regex(regex) != first_letter:
                    unrelated_regexes[level].append(regex)
        for level in unrelated_regexes:
            level_set = set(level_graph[level])
            for regex in unrelated_regexes[level]:
                level_set.remove(regex)
            level_graph[level] = list(level_set)


    """
    Returns the number of unique letters that are not '*', '(', or ')'
    """
    def count_unique_literals(self, expression):
        return len(set(expression.replace("*", "").replace("(", "").replace(")", "")))

    """
    Returns a string that is the closest regex that has the maximum amount of stars
    to the given expression. The function first creates the unique literals of the expression
    then turns it into a regular expression with the maximum amount of stars. 
    Ex. A*B*C* returns (A*B*C*)*
        BDBDBABC returns (A*B*D*C)*

    Ues the is_regex argument if there will be unwanted asterisks or parentheses 
    in the expression
    """
    def root(self, expression, is_regex=False):
        root_string = ""
        characters_in_order = []
        clean_expression = list(set(expression))
        if is_regex:
        	clean_expression = expression.replace('(', '').replace(')','').replace('*','')
        for char in sorted(clean_expression):
            root_string += char + '*'
        root_string = '(' + root_string + ')*'
        return root_string


    """
    returns the shortest regexes in the argument in a list
    """
    def only_shortest_regexes(self, regexes):
        shortest_length = len(min(regexes, key=len))
        return [regex for regex in regexes if len(regex)==shortest_length]

    """
    findregex() takes in any string with no more than 5 unique characters
    It will find all matching regex that have no matching children from
    the regex tree "bottomupresults*.txt" 
    Ex. findregex("AABC") = A*BC
    """
    def findregex(self, expression):
        encodekey = self.expression_encode_dict(expression)
        encoded_expression = self.translate_expression(encodekey, expression)
        # print("encoded_expression is " + encoded_expression) # debug

        # print("Building level graph...\n") # debug
        level_graph = self.level_graphs_[len(encodekey)-1].copy()

        self.remove_not_matching_first_letter_nodes(level_graph, encoded_expression[0])
        
        results = []
        levels_as_ints = [int(level) for level in level_graph.keys()]
        level = max(levels_as_ints)

        while level >= 0:
            # print('searching level ' + str(level)) # debug
            for regex in level_graph[str(level)]:
                # print('regex is ' + regex)
                if self.completematch(regex, encoded_expression):
                    results.append(regex)
            if results:
                break
            else:
                level -= 1

        # print("Found " + str(results) + " at level " + str(level)) # debug
        pruned_results = self.only_shortest_regexes(results)
        # print("\nWith only the shortest regular expressions the results are now:\n" + str(pruned_results)) # debug

        translated_results = []
        for result in pruned_results:
            translated = self.translate_encoded_expression(encodekey, result)
            translated_results.append(translated)

        return {regex : str(level) for regex in translated_results}  

def main():
    valid = False
    results = []
    try:
        this_categorizer = categorizer() 
    except FileNotFoundError:
        print(FileNotFoundError)
        sys.exit()
    else:
        if len(sys.argv) < 2:
            print('Enter at least one arg')
            sys.exit()
        for argument in sys.argv:
            if argument == __file__:
                pass
            elif re.match('^[\w-]+$', argument) is None or \
                this_categorizer.count_unique_literals(argument) > 5:
                print('Invalid argument ' + argument)
                sys.exit()
            else:
                results.append((argument, this_categorizer.findregex(argument)))

    for expression in results:
        print(expression[0] + ':')
        for regex in expression[1]:
            print('     ' + regex)

    return results

if __name__ == "__main__":
    main()
