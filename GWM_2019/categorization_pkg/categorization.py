#!/usr/bin/env
import os
import sys

from PyQt5.QtWidgets import QApplication

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from categorization_pkg.categorization_ui import Ui_categorization_widget
from categorization_pkg.categorization_view import categorization_view

"""
This file can be ran to create a ui of just the encoding widget.
It is not used when running gw_method.py
"""
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = Ui_categorization_widget()
    view = categorization_view(ui)
    ui.show()
    sys.exit(app.exec_())