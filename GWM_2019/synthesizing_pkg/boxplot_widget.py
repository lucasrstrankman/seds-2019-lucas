#!/usr/bin/env

from PyQt5 import QtWidgets
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np


class boxplotwidget(FigureCanvas):
    def __init__(self, regexes, gwfs, ylims=None, yscale=None,parent=None):
        fig = Figure()
        self.axes = fig.add_subplot(111)
        
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        self.axes.boxplot(gwfs)
        self.axes.set_ylabel("GWF")
        self.axes.yaxis.grid(True, linestyle='-', which='major', color='lightgrey')
        if ylims:
            if ylims[1] != ylims[0]:
                self.axes.set_ylim([ylims[0],ylims[1]])
        if yscale:
            self.axes.set_yscale(yscale)
        self.axes.set_xlabel("Regex")
        self.axes.set_xticks(range(1,len(regexes)+1))
        self.axes.set_xticklabels(regexes)

        self.axes.hold(False)
        FigureCanvas.setSizePolicy(self,
                QtWidgets.QSizePolicy.Preferred,
                QtWidgets.QSizePolicy.Preferred)
        FigureCanvas.updateGeometry(self)
