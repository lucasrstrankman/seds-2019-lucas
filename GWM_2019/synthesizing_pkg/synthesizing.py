#!/usr/bin/env
import os
import sys

from PyQt5.QtWidgets import QApplication

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from synthesizing_pkg.synthesizing_ui import Ui_synthesizing_widget
from synthesizing_pkg.synthesizing_view import synthesizing_view

"""
This file can be ran to create a ui of just the encoding widget.
It is not used when running gw_method.py
"""
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = Ui_synthesizing_widget()
    view = synthesizing_view(ui)
    ui.show()
    sys.exit(app.exec_())

