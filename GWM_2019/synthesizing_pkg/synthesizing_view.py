#!/usr/bin/env
import datetime
import os
from statistics import mean
import sys
import threading
import timeit

from PyQt5 import QtWidgets
from PyQt5.QtCore import QObject, pyqtSignal

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from synthesizing_pkg.synthesizing_help_dialog_ui import Ui_Dialog as help_dialog 
from synthesizing_pkg.merging_service import merging_service, validateinputfile, build_regex_dict
from synthesizing_pkg.boxplot_widget import boxplotwidget
from synthesizing_pkg.dict_translation_service import translation_service

class synthesizing_view(QObject):

    unknown_error = pyqtSignal(str, str)
    success_dialog_signal = pyqtSignal(str)
    update_plot_signal = pyqtSignal(dict)
    update_runtime = pyqtSignal(str)
    
    def __init__(self, ui):
        super(synthesizing_view, self).__init__()
        self.ui_ = ui
        self.merger_ = None
        self.translator_ = translation_service()
        self.ui_.file_explorerbutton.clicked.connect(self.handle_file_explorerbutton_clicked)
        self.ui_.startbutton.clicked.connect(self.handle_startbutton_clicked)
        self.ui_.updateplot_button.clicked.connect(self.handle_update_button_clicked)
        self.unknown_error.connect(self.showerrormessage)
        self.update_plot_signal.connect(self.update_plot)
        self.success_dialog_signal.connect(self.show_success_dialog)
        self.update_runtime.connect(self.ui_.runtime.setText)
        self.merged_regex_dict = None

    def handle_helpbutton_clicked(self):
        self.help_window = help_dialog()
        self.help_window.show()

    def handle_update_button_clicked(self):
        if not self.merged_regex_dict:
            return
        upperlimit = self.ui_.plot_upperlimit.value()
        lowerlimit = self.ui_.plot_lowerlimit.value()
        scale = None
        if self.ui_.plot_scale.currentText() == 'Linear':
            scale = 'linear'
        elif self.ui_.plot_scale.currentText() == 'Logarithmic':
            scale = 'log'
        elif self.ui_.plot_scale.currentText() == 'Symmetrical Log':
            scale = 'symlog'
        self.update_plot(self.merged_regex_dict, ylims=[lowerlimit, upperlimit], yscale=scale)

    def handle_file_explorerbutton_clicked(self):
        filepath = QtWidgets.QFileDialog.getOpenFileName()
        self.ui_.filepath_label.setText(filepath[0])

    def handle_startbutton_clicked(self):
        filepath = self.ui_.filepath_label.text()
        try:
            print('Validating Inputfile')
            validateinputfile(filepath)
        except ValueError as err:
            self.showerrormessage("File Error", str(err))
        else:
            self.ui_.startbutton.setEnabled(False)
            print('Removing Old Results')
            while self.ui_.results_table.rowCount():
                self.ui_.results_table.removeRow(0)
            t = threading.Thread(target=self.startprogram)
            t.start()

    def startprogram(self):
        output_filenames = None
        try:
            inputfile = self.ui_.filepath_label.text()
            print('Initializing Merger')
            self.merger_ = merging_service()
            #
            start_time = timeit.default_timer()
            # item_dict = build_item_dict(self.ui_.filepath_label.text())
            print('Building Regex Dict')
            regex_dict = build_regex_dict(self.ui_.filepath_label.text())
            print('Translating Regex Dict')
            translated_regex_dict, translation_legend = self.translator_.translate_dict(regex_dict)
            # print('translated_regex_dict is ' + str(translated_regex_dict))
            # print('translation_legend is ' + str(translation_legend))
            print('Merging Regex Dict')
            self.merger_.merge_regex_dict(translated_regex_dict)
            self.merged_regex_dict = self.merger_.regex_dict
            print('Untranslating Merged Regex Dict')
            self.merged_regex_dict = self.translator_.untranslate_dict(self.merged_regex_dict, translation_legend) 
            #
            end_time = timeit.default_timer()
            ms = (end_time-start_time) * 1000.0 % 1000
            m, s = divmod(end_time-start_time, 60)
            h, m = divmod(m, 60)
            string_runtime =  "%dh %02dm %02ds %03dms " % (h, m, s, ms)
            self.update_runtime.emit(string_runtime)
            #
            self.update_plot_signal.emit(self.merged_regex_dict)
            print('Populuating GUI')
            self.populate_results_table(self.merged_regex_dict)
            print('Writing to Outputfiles')
            output_filenames = self.create_outputfiles(inputfile, self.merged_regex_dict, self.merger_.merges)
            self.update_runtime_log(string_runtime, inputfile)
            #
            output_path = "/synthesizing_pkg/logs/"
            self.success_dialog_signal.emit("Output files can be found at \n" + 
                output_path + output_filenames[0] + '\n' + 
                output_path + output_filenames[1])
        except Exception as e:
            self.unknown_error.emit("Unknown Exception", str(e))
        self.ui_.startbutton.setEnabled(True)

    def populate_results_table(self, regex_dict):
        row = 0
        self.ui_.results_table.insertRow(row)
        # print('regex_dict is ' + str(regex_dict))
        sorted_regex_dict = sorted([(regex, 
                                     str(len(regex_dict[regex])), 
                                     str(round(mean(regex_dict[regex]), 4))) 
                                     for regex in regex_dict if regex_dict[regex]])
        for regex in sorted_regex_dict:
            self.ui_.results_table.setItem(row, 0, QtWidgets.QTableWidgetItem(regex[0]))
            self.ui_.results_table.setItem(row, 1, QtWidgets.QTableWidgetItem(regex[1]))
            self.ui_.results_table.setItem(row, 2, QtWidgets.QTableWidgetItem(regex[2]))
            row += 1
            self.ui_.results_table.insertRow(row)

    def update_plot(self, regex_dict, ylims=None, yscale=None):
        regexes = list(regex_dict.keys())
        gwfs = [list(regex_dict[regex]) for regex in regexes]
        
        boxplot = boxplotwidget(regexes, gwfs, ylims=ylims, parent=self.ui_.plotwidget, yscale=yscale)
        layout = self.ui_.plotwidget.layout()
        if layout.count():
            layout.itemAt(0).widget().setParent(None) # remove current plot
        self.ui_.plotwidget.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(boxplot)

    def create_outputfiles(self, inputpath, results, merges):
        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S ")
        output_filenames = []

        output_filenames.append(current_datetime + inputpath[inputpath.rfind('/')+1:inputpath.rfind('.')] + ' output_log.csv')
        output_filepath = os.path.dirname(os.path.realpath(__file__)) + '\\logs\\' + output_filenames[0]
                            
        with open(output_filepath, 'w') as outputfile:
            for merge in merges:
                outputfile.write(str(merge[0]) + ',' + str(merge[1]) + ',' + str(merge[2]) + '\n')
        
        output_filenames.append(current_datetime + inputpath[inputpath.rfind('/')+1:inputpath.rfind('.')] + '_final_output_log.csv')
        output_filepath = os.path.dirname(os.path.realpath(__file__)) + '\\logs\\' + output_filenames[1]
        with open(output_filepath, 'w') as outputfile:
            for result in results:
                outputfile.write(result + ',' + str(len(results[result])) + '\n')   

        return output_filenames

    def update_runtime_log(self, runtime, input_filepath):
        output_filepath = os.path.dirname(os.path.realpath(__file__)) + '\\synthesizing_runtimes.csv'
        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
        with open(output_filepath, 'a') as runtime_file:
            runtime_file.write(current_datetime + ',' +
                               input_filepath + ' ,' + 
                               runtime + '\n')


    def showerrormessage(self, title, message):
        error_message = QtWidgets.QMessageBox.critical(self.ui_, title, message)

    def show_success_dialog(self, message):
        success_dialog = QtWidgets.QMessageBox.information(self.ui_, "Synthesizing Successful", message)
