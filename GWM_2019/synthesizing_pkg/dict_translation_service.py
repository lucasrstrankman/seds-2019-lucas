#!/usr/bin/env
import os
import sys

"""
This class takes in a dict where every key is a regex
It then creates a corresponding dict where every literal of every key
Consists only of A, B, C, D, or E
It can then be called to reverse this process 
"""
class translation_service():

	def __init__(self):
		pass

	"""
	ARGS 
		untranslated_dict - A dict where the keys are regexes with operators (, ), or *
	RETURNS - A dict that maps every literal of the keys in dict to letters, ABCDE
			  There is no hardcap on how high in the ASCII table to go after A

	Ex. untranslated_dict = {'S*Y':5}
		legend = {'A':'S', 'B':Y} 
	"""
	def create_legend(self, untranslated_dict):
		legend = dict()
	
		current_letter = 'A'
		untranslated_letters = set()

		for regex in untranslated_dict:
			for char in set(regex):
				untranslated_letters.add(char)
		untranslated_letters.discard('*')
		untranslated_letters.discard(')')
		untranslated_letters.discard('(')

		for char in sorted(untranslated_letters):
			if char not in legend.items():
				legend[current_letter] = char
				current_letter = chr(ord(current_letter)+1)
		
		return legend

	"""
	ARGS 
		letter - A string that should be translated if in legend
		legend - a dict where the keys are from the input, and the 
				 values are the outputs
	RETURNS - The letter that the dict represents, otherwise return the original letter

	Ex. letter = 'A'
		legend = {'A':'S', 'B':'Y'}
		RETURNS 'S'

	Ex. letter = '*'
		legend = {'A':'S', 'B':'Y'}
		RETURNS '*'
	"""
	def replace_char(self, letter, legend):
		if letter in legend:
			return legend[letter]
		else:
			return letter

	"""
	ARGS 
		untranslated_dict - A dict where the keys are regexes with operators (, ), or *
	RETURNS 
		translated_dict - The original dict but the literals in the keys have been replaced by ABCDE
		legend - The legend create/used when translating the original dict
	Ex. untranslated_dict = {'(SY)*':[5,5], 'YS*:[5,5]'}
		translated_dict = {'(AB)*':[5,5], 'BA*':[5,5]}
		legend = {'A':'S', 'B','Y'}	
	"""
	def translate_dict(self, untranslated_dict):
		translated_dict = dict()
		legend = self.create_legend(untranslated_dict)
		
		inv_legend = {v:k for k,v in legend.items()}
		for regex in untranslated_dict:
			translated_regex = ""
			for char in regex:
				translated_regex += (self.replace_char(char, inv_legend))
			translated_dict[translated_regex] = untranslated_dict[regex]

		return translated_dict, legend

	"""
	Using the output of translate_dict
	Gives the original input of translate_dict
	
	Ex. translated_dict = {'(AB)*':[5,5], 'BA*':[5,5]}
		legend = {'A':'S', 'B','Y'}	
		untranslated_dict = {'(SY)*':[5,5], 'YS*:[5,5]'}
	"""
	def untranslate_dict(self, translated_dict, legend):
		untranslated_dict = dict()
		for regex in translated_dict:
			untranslated_regex = ""
			for char in regex:
				untranslated_regex += self.replace_char(char, legend)
			untranslated_dict[untranslated_regex] = translated_dict[regex]

		return untranslated_dict
