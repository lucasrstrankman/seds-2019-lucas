#!/usr/bin/env
import collections
import os
import queue
from operator import itemgetter
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from synthesizing_pkg.node_selector import node_selector
from synthesizing_pkg.stats import r_utest
from synthesizing_pkg.dict_translation_service import translation_service

"""
For further information regarding this process, look at "Merging Process.pdf"
"""
class merging_service():

    def __init__(self):
        self.selector_ = None
        self.level_graph = dict()
        self.distance_dict = dict()
        self.parent_tree = dict()
        self.child_tree = dict()
        self.decided_set = set()
        self.regex_dict = dict()
        self.test_results = dict()
        self.merges = list()

    """
    Once this function has been called. A minimal sub hierarchy will first be created.
    Then self.regex_dict will continously merge its nodes until no merges are left to be done
    """
    def merge_regex_dict(self, regex_dict):
        self.regex_dict = regex_dict
        # print('filled nodes are ' + str(self.regex_dict.keys()))
        self.selector_ = node_selector(self.regex_dict.keys())
        self.parent_tree = self.selector_.minimal_parent_tree
        self.child_tree = self.selector_.minimal_child_tree
        self.level_graph = self.selector_.level_graph
        for level in self.level_graph:
            for regex in self.level_graph[level]:
                self.distance_dict[regex] = level
        # print('artificial_nodes are ' + str(self.selector_.artificial_nodes))
        for artificial_node in self.selector_.artificial_nodes:
            self.regex_dict[artificial_node] = []
        self.fill_test_results()
        # print('test_results are ')
        # for regex in self.test_results:
        #     print(regex + ' : ')
        #     for parent_or_sibling in self.test_results[regex]:
        #         print('     ' + parent_or_sibling + ' : ' + str(self.test_results[regex][parent_or_sibling]))
        self.merge_regex_dict_recursively()

    """
    Continuously merges nodes in self.regex_dict until all self.select_node returns None
    select_node will return None when no undecided nodes are left
    """
    def merge_regex_dict_recursively(self):
        NODE = self.select_node()
        if not NODE:
            print("Finished Merging")
            print('Final nodes are ' + str(self.regex_dict.keys()))
            return
        at_least_one_accepted = False
        for parent_or_sibling in self.test_results[NODE]:
            if self.test_results[NODE][parent_or_sibling] >= 0.05:
                print('NODE accepted ' + parent_or_sibling)
                at_least_one_accepted = True
                break
        if at_least_one_accepted:
            print(NODE + ' has at least one accepted test')
            best_parent = None
            for sibling_or_parent in self.test_results[NODE]:
                if sibling_or_parent in self.parent_tree.get(NODE):
                    if not best_parent:
                        best_parent = sibling_or_parent
                    if self.test_results[NODE][sibling_or_parent] > \
                        self.test_results[NODE][best_parent]:
                        best_parent = sibling_or_parent
            if not best_parent:
                best_parent = self.parent_tree[NODE][0]
            print(best_parent + ' is the best parent')
            self.merge_regexes(NODE, best_parent)
        else:
            print(NODE + ' has no accepted tests')
            self.decided_set.add(NODE)
        self.merge_regex_dict_recursively()

    """
    Selects a node by the criteria in the following order
        - the node is on the lowest level with undecided nodes
        - the node has no siblings
        - the node has no parents
        - the node has the most accepted tests with surroudning nodes in self.test_results

    If there are no nodes left that are decided, None is returned.

    So if node A and node B are on level 3 (which is the bottom most level)
    And A has no siblings, while B has no parents but has siblings
    node A will be returned
    """
    def select_node(self):
        highest_undecided_level = max(self.level_graph.keys())
        undecided_nodes_in_level = [x for x in self.level_graph[highest_undecided_level] if x not in self.decided_set]
        undecided_filled_nodes_in_level = [x for x in undecided_nodes_in_level if self.regex_dict.get(x)]
        while not undecided_filled_nodes_in_level:
            highest_undecided_level -= 1
            if highest_undecided_level < 0:
                return
            undecided_nodes_in_level = [x for x in self.level_graph[highest_undecided_level] if x not in self.decided_set]
            undecided_filled_nodes_in_level = [x for x in undecided_nodes_in_level if self.regex_dict.get(x)]

        undecided_filled_nodes_in_level = sorted(undecided_filled_nodes_in_level)
        print('\nhighest_undecided_level is ' + str(highest_undecided_level))
        print('undecided_filled_nodes_in_level is ' + str(undecided_filled_nodes_in_level))
        for node in undecided_filled_nodes_in_level:
            if not self.find_siblings(node):
                print('\nSelected ' + node + ' no siblings')
                return node

        for node in undecided_filled_nodes_in_level:
            if len(set(self.parent_tree[node]).intersection(set(self.regex_dict.keys()))) == 1:
                print('\nSelected ' + node + ' one parent')
                return node

        most_accepted = -1
        most_accepted_node = None
        for node in undecided_filled_nodes_in_level:
            amount_accepted = 0
            for sibling_or_parent in self.test_results[node]:
                if self.test_results[node][sibling_or_parent] >= 0.05:
                    amount_accepted += 1
            if amount_accepted > most_accepted:
                most_accepted = amount_accepted
                most_accepted_node = node
        print('\nSelected ' + str(most_accepted_node) + ' ' + str(most_accepted) + ' accepted null hypothesis')
        return most_accepted_node

    """
    For all the regexes that are in self.regex_dict 
    it conducts the mann-whitney U test using the data found in self.regex_dict
    and stores the resulting p-value in self.text_results
    """
    def fill_test_results(self):
        for regex in [regex for regex in self.regex_dict if regex not in self.decided_set]:
            self.test_results[regex] = dict()
        for regex in [regex for regex in self.regex_dict if regex not in self.decided_set]:
            siblings = list(self.find_siblings(regex))
            parents = self.parent_tree.get(regex, [])
            siblings_and_parents = siblings + parents
            for sibling_or_parent in siblings_and_parents:
                if sibling_or_parent not in self.test_results[regex]:
                    test_result = self.p_value(regex, sibling_or_parent)
                    # print('     ' + regex + ' + ' + sibling_or_parent + ' = ' + str(test_result))
                    self.test_results[regex][sibling_or_parent] = test_result
                    self.test_results[sibling_or_parent][regex] = test_result

    """
    Finds all affected regexes by a merge and updates their test results.
    The regex that has been lost due to a merge is removed from being found
    in the test results.
    Affected regexes are the children, siblings, and parents of the changed regexes
    Only completes a test if the regex is not decided and is in  self.regex_dict
    """
    def update_test_results(self, changed_regexes, merged_regex):
        for related_node in self.test_results[merged_regex]:
            self.test_results[related_node].pop(merged_regex)
        self.test_results.pop(merged_regex)

        for regex in changed_regexes:
            for related_node in self.test_results[regex]:
                self.test_results[related_node].pop(regex)
            self.test_results[regex] = dict()

        for regex in changed_regexes:
            siblings = list(self.find_siblings(regex))
            parents = self.parent_tree.get(regex, [])
            children = self.child_tree.get(regex, [])
            related_nodes = [node for node in siblings + parents + children if node not in self.decided_set]
            for related_node in related_nodes:
                if related_node in self.regex_dict:
                    if related_node not in self.test_results:
                        self.test_results[related_node] = dict()
                    if related_node not in self.test_results[regex]:
                        test_result = self.p_value(regex, related_node)
                        # print('     ' + regex + ' + ' + related_node + ' = ' + str(test_result))
                        self.test_results[regex][related_node] = test_result
                        self.test_results[related_node][regex] = test_result
                print('skipping ' + related_node)

    """
    Merges a child regex into a parent regex
    The children of the child regex will be inherited by the parent regex
    The data of the child regex will be inhertied by the parent regex
    When a merge is made, it is kept track of in self.merges
    Updates test results of affected nodes
    """
    def merge_regexes(self, child_regex, parent_regex):
        print('merging ' + child_regex + ' into ' + parent_regex)
        changed_regexes = [parent_regex]

        data1 = self.regex_dict.get(child_regex, [])
        data2 = self.regex_dict.get(parent_regex, [])
        data = data1 + data2
        self.merges.append((child_regex, parent_regex, self.test_results[child_regex][parent_regex]))

        self.regex_dict.pop(child_regex)
        self.regex_dict[parent_regex] = data

        children = self.child_tree.get(child_regex, [])
        for child in children:
            if self.regex_dict.get(child):
                changed_regexes.append(child)
                self.decided_set.discard(child)
                self.parent_tree[child].remove(child_regex)
                if child not in self.child_tree[parent_regex]:
                    # print('     ' + child + ' is now a child of ' + parent_regex)
                    self.child_tree[parent_regex].append(child)
                if parent_regex not in self.parent_tree[child]:
                    self.parent_tree[child].append(parent_regex)
                old_level = self.distance_dict[child]
                new_level = self.distance_dict[child_regex]
                if old_level != new_level:
                    self.distance_dict[child] = new_level
                    self.level_graph[old_level].remove(child)
                    self.level_graph[new_level].append(child)

        # test_results = dict()
        # self.fill_test_results()

        self.update_test_results(changed_regexes, child_regex)

    """
    For two regexes, using the associated data in self.regex_dict
    performs a mann_whitney test through R 
    Uses exact settings if the size of the data is over 20
    Uses non exact settings if the size of the data is under 20    
    """
    def p_value(self, regex1, regex2):
        data1 = [x for x in self.regex_dict.get(regex1, [])]
        data2 = [x for x in self.regex_dict.get(regex2, [])]
        if len(data1)+len(data2) > 20:
            return r_utest(data1, data2, tol=0, exact='FALSE')['p']
        else:
            return r_utest(data1, data2, tol=0, exact='TRUE')['p']

    """
    For a given regex, returns the siblings in a set as strings
    Siblings meaning regexes that share a mutual parent
    """
    def find_siblings(self, regex):
        siblings = set()
        if regex not in self.parent_tree:
            return siblings
        else:
            for parent in set(self.parent_tree[regex]).intersection(set(self.regex_dict.keys())):
                for child in set(self.child_tree[parent]).intersection(set(self.regex_dict.keys())):
                    siblings.add(child)
        try:
            siblings.remove(regex)
        except KeyError:
            pass
        return siblings


"""
returns a dict where dict[item] = gwf, regex
"""
def build_item_dict(filepath):
    item_dict = dict()
    with open(filepath, 'r') as f:
        data = f.read().splitlines()
        for line in data:
            line = line.split()
            item_dict[line[0]] = (float(line[2]), line[1])
    return item_dict

def build_regex_dict(filepath):
    regex_dict = dict()
    with open(filepath, 'r') as f:
        data = f.read().splitlines()
        for line in data:
            line = line.split()
            regex_dict[line[0]] = [float(gwf) for gwf in line[1:]]
    return regex_dict

def getvalid_regexes(filepath):
    unique_literals = set()
    with open(filepath, 'r') as f:
        filecontent = f.read().splitlines()
        for line in filecontent:
            regex = line.split()[0].replace('*', '').replace('(', '').replace(')', '')
            unique_literals = unique_literals.union(set(regex))
    unique_literals.discard('*')
    unique_literals.discard('(')
    unique_literals.discard(')')
    PATH = os.path.dirname(os.path.abspath(__file__))
    DATAFILE = PATH + '/bottomupresults' + str(len(unique_literals)) + '.txt'
    # print('validitng file is ' + '/bottomupresults' + str(len(unique_literals)) + '.txt')
    filecontent = open(DATAFILE, 'r').read().splitlines()[:-1]
    valid_regexes = set()
    for line in filecontent:
        if 'parents' not in line and 'children' not in line:
            valid_regexes.add(line.strip().split()[0])
    return valid_regexes

"""
Every line in the input file must be in the format
ROWID REGEX GWF
Where the REGEX must be a valid regex
"""
def validateinputfile(filepath):
    translator = translation_service()
    translated_regex_dict, legend = translator.translate_dict(build_regex_dict(filepath))
    if len(legend.keys()) > 5:
        raise ValueError('Too many unique letters in regexes')
    valid_regexes = getvalid_regexes(filepath)

    for regex in translated_regex_dict:
        if regex not in valid_regexes:
            untranslated_regex = ""
            for char in regex:
                untranslated_regex += translator.replace_char(char, legend)
            raise ValueError(untranslated_regex + ' Regex invalid')

    with open(filepath, 'r') as f:
        alldata = f.read().splitlines()
        linenumber = 0
        itemlist = []
        for line in alldata:
            linenumber += 1
            data = line.split()
            if len(data) < 2:
                raise ValueError('Line ' + str(linenumber) + ' Need at least one datum')
            # regex = data[0]
            # if regex not in valid_regexes:
            #     raise ValueError('Line ' + str(linenumber) + ' Regex invalid')
            gwfs = data[1:]
            for gwf in gwfs:
                try:
                    float(gwf)
                except ValueError as e:
                    raise ValueError('Line ' + str(linenumber) + ' GWF not float')

"""
prints the output to a file as well as to the screen
"""
def print_results(item_dict, filepath):
    outputfile = open(filepath, 'w')
    merged_items = sorted([(item, item_dict[item][0], item_dict[item][1]) for item in item_dict])
    for item in merged_items:
        outputfile.write(item[0] + ' ' + str(item[1]) + ' ' + str(item[2]) + '\n')
        # print(item[0] + ' ' + str(item[1]) + ' ' + str(item[2]))
    outputfile.close()


def main():
    inputfile = 'testfiles/smalltestfile.txt'
    outputfile = 'testoutput.txt'
    try:
        validateinputfile(inputfile)
    except ValueError as err:
        print('Input file issue\n' + str(err))
        return

    merger = merging_service()
    regex_dict = build_regex_dict(inputfile)
    merger.merge_regex_dict(regex_dict)
    merged_regex_dict = merger.regex_dict

    # item_dict = build_item_dict(inputfile)
    # merger = merging_service()
    # merged_item_dict = merger.merge_item_dict(item_dict)

    # print_results(merged_item_dict, outputfile)

if __name__ == '__main__':
    main()
