#!/usr/bin/env
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

class node_selector():

    PATH = os.path.dirname(os.path.abspath(__file__))

    """
    Upon intialization with filled nodes.
    This class will create a minimal sub hierarchy and through member variables
    A large amount of information about the filled nodes are available
    This class selects nodes so that every node has a parent to merge into
    in exception of the root

    Filled_nodes are given
    relevant nodes are the combination of artificial and filled nodes
    Artifical_nodes are nodes required so that all relevant nodes have parents to merge into
    """
    def __init__(self, filled_nodes):
        self.DATAFILE_ = self.PATH + '/bottomupresults' + str(self.count_unique_literals(filled_nodes)) + '.txt'
        self.parent_tree = self.build_parent_tree() # {string : set}
        self.child_tree = self.build_child_tree() # {string : set}
        self.level_graph = self.build_level_graph() # {string : list}
        self.inv_level_graph = self.build_inv_level_graph()
        self.filled_nodes = list(filled_nodes)
        self.relevant_nodes = set(filled_nodes)
        self.artificial_nodes = set()
        self.minimal_level_graph = dict()
        self.minimal_child_tree = dict()
        self.minimal_parent_tree = dict()
        self.find_artificial_nodes()
        self.find_minimal_trees()
        # print('Relevant nodes : ' + str(self.relevant_nodes))
        # print('Artifical nodes : ' + str(self.artificial_nodes))
        # print('minimal_child_tree : ')
        # for regex in self.minimal_child_tree:
            # print('     ' + regex + ':' + str(self.minimal_child_tree[regex])) 
        # print('minimal_parent_tree : ')
        # for regex in self.minimal_parent_tree:
            # print('     ' + regex + ':' + str(self.minimal_parent_tree[regex])) 

    """
    Using the original parent and child trees, it removes the nodes that are not considered relevant
    After removing the nodes, the parent and child trees are then minimal
    """
    def find_minimal_trees(self):
        for node in self.relevant_nodes:
            if node in self.parent_tree:
                minimal_parents = list(self.relevant_nodes.intersection(self.parent_tree[node]))
                if minimal_parents:
                    self.minimal_parent_tree[node] = minimal_parents             
            if node in self.child_tree:
                minimal_children = list(self.relevant_nodes.intersection(self.child_tree[node]))
                if minimal_children:
                    self.minimal_child_tree[node] = minimal_children


    """
    Counts the number of unique literals in a regex
    ex. (AB)* = 2, for A and B
        SYSYSDDDD = 3, for SYD
    """
    def count_unique_literals(self, regexes):
        unique_letters = set()
        for regex in regexes:
            unique_letters = unique_letters.union(set(regex))
        unique_letters.discard('(')
        unique_letters.discard(')')
        unique_letters.discard('*')
        # print('unique letters is ' + str(len(unique_letters)))
        return len(unique_letters)

    def find_artificial_nodes(self):
        level = max(self.level_graph.keys())
        while level >= 0:
            # print('level ' + str(level))
            for regex in self.sorted_nodes(self.level_graph[level]):
                # print('     regex ' + regex)
                if regex in self.relevant_nodes:
                    pass
                else:
                    relevant_regex_children = set(self.child_tree.get(regex)).intersection(self.relevant_nodes) 
                    if relevant_regex_children:
                        # print('         relevant children ' + str(relevant_regex_children))
                        for node in relevant_regex_children:
                            if not self.parent_tree[node].intersection(self.relevant_nodes):
                                self.relevant_nodes.add(regex)
                                self.artificial_nodes.add(regex)
                                # print('         ' + node + ' no relevant parents')
                                # print('         adding')
                                break
            level -= 1

    """
    Given a list of nodes, it sorts the nodes by the priorities from highest to loweest
        - nodes with no siblings 
        - nodes with one parent 
        - nodes with no children
        - then the rest of the nodes

    All of nodes that are the same priority are then sorted by lexicographical order
    """
    def sorted_nodes(self, the_nodes):
        nodes = list(the_nodes.copy())
        no_siblings = list()
        one_parent = list()
        no_children = list()
        for node in nodes:
            if not self.find_siblings(node):
                no_siblings.append(node)
                nodes.remove(node)
            elif len(self.parent_tree.get(node)) == 1:
                one_parent.append(node)
                nodes.remove(node)
            elif node not in self.child_tree:
                no_children.append(node)
                nodes.remove(node)
        return sorted(no_siblings) + \
               sorted(one_parent) + \
               sorted(no_children) + \
               sorted(nodes)

    """
    Given the input DATAFILE_, this function returns a dict where
    Each key is a level, and the values are the nodes in those levels

    A level is defined by regexes shortest distance to the root
    """
    def build_level_graph(self):
        filecontent = open(self.DATAFILE_, 'r').read().splitlines()[:-1]
        level_graph = dict()

        for line in filecontent:
            if 'parents' in line or 'children' in line:
                pass
            else:
                level = int(line.split()[1])
                regex = line.split()[0]
                if level not in level_graph:
                    level_graph[level] = list()
                level_graph[level].append(regex)
        return level_graph

    """
    Returns a dict where each key is a regex, and the value is the level
    it belongs in
    """
    def build_inv_level_graph(self):
        inv_level_graph = dict()
        for level in self.level_graph:
            for regex in self.level_graph[level]:
                inv_level_graph[regex] = int(level)
        return inv_level_graph

    """
    Build a dict where each key is a regex, and the values are the 
    parents of that regex
    If a regex has no parents, it is not a key in the return value
    """
    def build_parent_tree(self):
        filecontent = open(self.DATAFILE_, 'r').read().splitlines()[:-1]
        parent_tree = dict()
        activechildregex = ''

        for line in filecontent:
            if 'parents' in line:
                regexes = line[16:-1].replace("'", "").replace(",", "").split()
                for regex in regexes:
                    parent_tree[activechildregex].add(regex)
            elif 'children' not in line:
                activechildregex = line.strip().split()[0]
                parent_tree[activechildregex] = set()
        return parent_tree

    """
    Build a dict where each key is a regex, and the values are the 
    children of that regex
    If a regex has no children, it is not a key in the return value
    """
    def build_child_tree(self):
        filecontent = open(self.DATAFILE_, 'r').read().splitlines()[:-1]
        child_tree = dict()
        activeparentregex = ''

        for line in filecontent:
            if 'children' in line:
                regexes = line[17:-1].replace("'", "").replace(",", "").split()
                for regex in regexes:
                    child_tree[activeparentregex].add(regex)
            elif 'parents' not in line:
                activeparentregex = line.strip().split()[0]
                child_tree[activeparentregex] = set()
        return child_tree

    """
    For a given regex, returns the siblings in a set as strings
    Siblings meaning regexes that share a mutual parent
    """
    def find_siblings(self, regex):
        siblings = set()
        if regex not in self.parent_tree:
            return siblings
        else:
            for parent in self.parent_tree[regex]:
               if parent in self.child_tree:
                    for child in self.child_tree[parent]:
                        siblings.add(child)
        # print('siblings is ' + str(siblings))
        try:
            siblings.remove(regex)
        except KeyError:
            pass
        return siblings

def main():
    filled = []
    if len(sys.argv) < 2:
        print('Enter at least one arg')
        sys.exit()
    for argument in sys.argv:
        if argument == __file__:
            pass
        else:
            filled.append(argument)
    try:
        this_node_selector = node_selector(filled)
    except FileNotFoundError:
        print(FileNotFoundError)
        sys.exit()

    print('Relevant nodes : ' + str(this_node_selector.relevant_nodes))
    print('Artifical nodes : ' + str(this_node_selector.artificial_nodes))
    print('minimal_child_tree : ')
    for regex in this_node_selector.minimal_child_tree:
        print(regex + ':' + str(this_node_selector.minimal_child_tree[regex])) 
    print('minimal_parent_tree : ')
    for regex in this_node_selector.minimal_parent_tree:
        print(regex + ':' + str(this_node_selector.minimal_parent_tree[regex])) 

if __name__ == '__main__':
    main()