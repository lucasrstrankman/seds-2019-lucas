#!/usr/bin/env
from PyQt5.QtCore import QObject

"""
This class controls the functiolities of the main window
Currently the only thing it does is ensure the correct help dialog is displayed according to the current tab
"""
class gw_method_view(QObject):

	def __init__(self, ui, encoding_view, categorization_view, synthesizing_view, fitness_of_statistical_tests_view):
		super(gw_method_view, self).__init__()
		self.ui_ = ui
		self.encoding_view_ = encoding_view
		self.categorization_view_ = categorization_view
		self.synthesizing_view_ = synthesizing_view
		self.fitness_of_statistical_tests_view_ = fitness_of_statistical_tests_view
		self.ui_.helpbutton.clicked.connect(self.encoding_view_.handle_helpbutton_clicked)
		# connect the signal of changing tabs to the function reconnect_help_button
		self.ui_.tabBar.currentChanged.connect(self.reconnect_help_button)

	"""
	This function disconnects the signal of clicking the help button from the current help dialog
	It then reconnects it to the correct help dialog
	"""
	def reconnect_help_button(self, tabNumber):
		self.ui_.helpbutton.clicked.disconnect()
		if tabNumber == 0:
			self.ui_.helpbutton.clicked.connect(self.encoding_view_.handle_helpbutton_clicked)
		if tabNumber == 1:
			self.ui_.helpbutton.clicked.connect(self.categorization_view_.handle_helpbutton_clicked)
		if tabNumber == 2:
			self.ui_.helpbutton.clicked.connect(self.synthesizing_view_.handle_helpbutton_clicked)
		if tabNumber == 3:
			self.ui_.helpbutton.clicked.connect(self.fitness_of_statistical_tests_view_.handle_helpbutton_clicked)
