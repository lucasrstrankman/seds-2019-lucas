#!/usr/bin/env
from PyQt5 import QtCore, QtGui, QtWidgets

"""
This class is the widget for the main window.
Usually this kind of file is generated using pyuic5
But in this case, this class has been hand written

Upon instantiation, it creates a tabwidget and fills it with the given widget arguments
It also creates a help button. 
"""
class Ui_gw_method_window(QtWidgets.QWidget):
    def __init__(self, fitness_of_statistical_tests_widget, encoding_widget, synthesizing_widget, categorization_widget):
        QtWidgets.QWidget.__init__(self)
        self.helpbutton = QtWidgets.QPushButton("Help")
        self.helpbutton.setMaximumWidth(70)
        self.tabBar = QtWidgets.QTabWidget()
        self.tabBar.addTab(encoding_widget, "Encoding")
        self.tabBar.addTab(categorization_widget, "Categorization")
        self.tabBar.addTab(synthesizing_widget, "Synthesizing")
        self.tabBar.addTab(fitness_of_statistical_tests_widget, "Fitness of Statistical Tests")
        self.setWindowTitle('GWM Support Tool ©')

        layout = QtWidgets.QGridLayout()
        self.setLayout(layout)
        self.layout().addWidget(self.helpbutton, 0, 1, QtCore.Qt.AlignRight)
        self.layout().addWidget(self.tabBar, 1, 0, 1, 2)