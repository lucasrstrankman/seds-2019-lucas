#!/usr/bin/env
import datetime
import os
import sys
import threading
import timeit

from PyQt5 import QtWidgets
from PyQt5.QtCore import QObject, pyqtSignal
from statsmodels.stats.diagnostic import kstest_normal
import scipy

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from fitness_of_statistical_tests_pkg.fitness_of_statistical_tests_help_dialog_ui import Ui_Dialog as help_dialog 
from fitness_of_statistical_tests_pkg.bestdistribution import bestdistribution
from fitness_of_statistical_tests_pkg.plotcanvaswidget import plotcanvaswidget
from fitness_of_statistical_tests_pkg import scottknott_interface as sk

class fitness_of_statistical_tests_view(QObject):

    unknown_error = pyqtSignal(str, str)
    success_dialog_signal = pyqtSignal()
    update_runtime = pyqtSignal(str)

    def __init__(self, ui):
        super(fitness_of_statistical_tests_view, self).__init__()
        self.ui_ = ui
        self.results = dict()
        self.distributions = dict()
        self.ui_.file_explorerbutton.clicked.connect(self.handle_file_explorerbutton_clicked)
        self.ui_.startbutton.clicked.connect(self.handle_startbutton_clicked)
        self.ui_.regularexpressions_list_L.selectionModel().currentChanged.connect(self.handle_regex_selection_L)
        self.ui_.regularexpressions_list_R.selectionModel().currentChanged.connect(self.handle_regex_selection_R)
        self.ui_.updateplot_button.clicked.connect(self.updateplot_button_clicked)
        self.ui_.distributions_button.clicked.connect(self.handle_distributions_button_clicked)
        self.ui_.scottknott_button.clicked.connect(self.scottknott_button_clicked)
        self.update_runtime.connect(self.ui_.runtime.setText)
        self.success_dialog_signal.connect(self.show_success_dialog)
        self.unknown_error.connect(self.showerrormessage)

    def scottknott_button_clicked(self):
        self.ui_.statistical_stackedwidget.setCurrentIndex(1)

    def handle_distributions_button_clicked(self):
        self.ui_.statistical_stackedwidget.setCurrentIndex(0)

    def handle_helpbutton_clicked(self):
        self.help_window = help_dialog()
        self.help_window.show()

    def handle_file_explorerbutton_clicked(self):
        filepath = QtWidgets.QFileDialog.getOpenFileName()
        self.ui_.filepath_label.setText(filepath[0])

    def handle_startbutton_clicked(self):
        filepath = self.ui_.filepath_label.text()
        if self.validatefile(filepath):
            self.clear_ui()
            if self.ui_.plotwidget.layout().count():
                self.ui_.plotwidget.layout().itemAt(0).widget().setParent(None) # remove current plot
            self.ui_.startbutton.setEnabled(False)
            t = threading.Thread(target=self.startprogram)
            t.setDaemon(True)
            t.start()


    def handle_regex_selection_L(self):
        self.updateplot()
        self.update_twosided_ks()
        self.update_totalitems_L()
        self.updatenormality_L()
        self.update_distribution_L()

    def handle_regex_selection_R(self):
        self.updateplot()
        self.update_twosided_ks()
        self.update_totalitems_R()
        self.updatenormality_R()
        self.update_distribution_R()

    def updateplot_button_clicked(self):
        self.updateplot()

    def clear_ui(self):
        self.ui_.regularexpressions_list_L.clear()
        self.ui_.regularexpressions_list_R.clear()
        self.ui_.ksstat_L.setText('0')
        self.ui_.ksstat_R.setText('0')
        self.ui_.ksstat_double.setText('0')
        self.ui_.pvalue_L.setText('0')
        self.ui_.pvalue_R.setText('0')
        self.ui_.pvalue_double.setText('0')
        self.ui_.bestdistribution_L.setText('n/a')
        self.ui_.bestdistribution_R.setText('n/a')
        self.ui_.totalitems_L.setText('0')
        self.ui_.totalitems_R.setText('0')

    def startprogram(self):
        try:
            start_time = timeit.default_timer()

            filepath = self.ui_.filepath_label.text()
            self.results = self.build_file(filepath)
            self.sk_input = self.build_sk_file(filepath)
            sk_results = sk.scottknott(self.sk_input)
            print('sk_results are ' + str(sk_results))
            self.populate_sk_table(sk_results)
            self.calculate_distributions()
            for regex in self.results:
                self.ui_.regularexpressions_list_L.addItem(regex)
                self.ui_.regularexpressions_list_R.addItem(regex)
            end_time = timeit.default_timer()
            ms = (end_time-start_time) * 1000.0 % 1000
            m, s = divmod(end_time-start_time, 60)
            h, m = divmod(m, 60)
            string_runtime =  "%dh %02dm %02ds %03dms " % (h, m, s, ms)
            self.update_runtime.emit(string_runtime)
            self.update_runtime_log(string_runtime)
        except Exception as e:
            self.unknown_error.emit("Unknown Exception", str(e))
        self.ui_.startbutton.setEnabled(True)
        self.success_dialog_signal.emit()

    def build_file(self, filepath):
        inputdata = open(filepath, 'r').read().splitlines()
        regex_dict = dict()
        for line in inputdata:
            linedata = line.split()
            regex = linedata[0]
            gwfs = [float(gwf) for gwf in linedata[1:]]
            regex_dict[regex] = gwfs
        return regex_dict

    def build_sk_file(self, filepath):
        inputdata = open(filepath, 'r').read().splitlines()
        regex_list = []
        with open(filepath, 'r') as f:
            data = f.read().splitlines()
            for line in data:
                linedata = line.split()
                regex = linedata[0]
                gwfs = [float(gwf) for gwf in linedata[1:]]
                regex_list.append((regex, gwfs))
        return regex_list


    def populate_sk_table(self, sk_results):
        row = 0
        table = self.ui_.scottknott_table
        for sk_result in sk_results:
            table.insertRow(row)
            table.setItem(row, 0, QtWidgets.QTableWidgetItem(str(sk_result.regex)))
            table.setItem(row, 1, QtWidgets.QTableWidgetItem(str(sk_result.mean)))
            table.setItem(row, 2, QtWidgets.QTableWidgetItem(str(sk_result.rank)))
            row += 1

    def validatefile(self, filepath):
        regexfile = []
        linenumber = 0
        try:
            regexfile = open(filepath, 'r').read().splitlines()
        except FileNotFoundError:
            self.showerrormessage("File Error", "File not found")
            return False
        for line in regexfile:
            linenumber += 1
            line = line.split()
            if len(line) < 2:
                self.showerrormessage("File Error", "Line " + str(linenumber) + " need GWF")
                return False
            else:
                for gwf in line[1:]:
                    try:
                        float(gwf)
                    except ValueError:
                        self.showerrormessage("File Error", "Line " + str(linenumber) + " gwf not number")
                        return False
        return True
  
    def update_runtime_log(self, runtime):
        output_filepath = os.path.dirname(os.path.realpath(__file__)) + '\\fitness_of_statistical_tests_runtimes.csv'
        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
        input_filepath = self.ui_.filepath_label.text() 
        with open(output_filepath, 'a') as runtime_file:
            runtime_file.write(current_datetime + ',' +
                               input_filepath + ' ,' + 
                               runtime + '\n')

    def updateplot(self):
        if not self.ui_.regularexpressions_list_L.currentItem() or \
            not self.ui_.regularexpressions_list_R.currentItem():
            return
        regexL = self.ui_.regularexpressions_list_L.currentItem().text()
        regexR = self.ui_.regularexpressions_list_R.currentItem().text()
        dataL = self.results[regexL]  
        dataR = self.results[regexR]  
        plot = plotcanvaswidget(dataL, regexL, dataR, regexR, self.ui_.plotwidget, 
            bins=self.ui_.bins.value(), width=10, height=10, dpi=50)
        
        self.ui_.plotwidget.setContentsMargins(1, 1, 1, 1)
        layout = self.ui_.plotwidget.layout()
        if layout.count():
            layout.itemAt(0).widget().setParent(None) # remove current plot
        layout.addWidget(plot)

    def update_twosided_ks(self):
        if not self.ui_.regularexpressions_list_L.currentItem() or \
            not self.ui_.regularexpressions_list_R.currentItem():
            return
        regexL = self.ui_.regularexpressions_list_L.currentItem().text()
        regexR = self.ui_.regularexpressions_list_R.currentItem().text()
        dataL = self.results[regexL]  
        dataR = self.results[regexR]  
        if dataL == dataR:
            self.ui_.ksstat_double.setText("N/A")
            self.ui_.pvalue_double.setText("N/A")
        else:            
            result = scipy.stats.ks_2samp(dataL, dataR)
            ksstat = result[0]
            pvalue = result[1]
            self.ui_.ksstat_double.setText(str(ksstat))
            self.ui_.pvalue_double.setText(str(pvalue))

    def updatenormality_L(self):
        regex = self.ui_.regularexpressions_list_L.currentItem().text()
        data = self.results[regex]
        result = kstest_normal(data)
        self.ui_.ksstat_L.setText(str(result[0]))
        self.ui_.pvalue_L.setText(str(result[1]))

    def update_distribution_L(self):
        regex = self.ui_.regularexpressions_list_L.currentItem().text()
        self.ui_.bestdistribution_L.setText(self.distributions[regex])

    def update_totalitems_L(self):
        regex = self.ui_.regularexpressions_list_L.currentItem().text()
        regexinfo = self.results[regex]
        self.ui_.totalitems_L.setText(str(len(regexinfo)))

    def updatenormality_R(self):
        regex = self.ui_.regularexpressions_list_R.currentItem().text()
        data = self.results[regex]
        result = kstest_normal(data)
        self.ui_.ksstat_R.setText(str(result[0]))
        self.ui_.pvalue_R.setText(str(result[1]))

    def update_distribution_R(self):
        regex = self.ui_.regularexpressions_list_R.currentItem().text()
        self.ui_.bestdistribution_R.setText(self.distributions[regex])

    def update_totalitems_R(self):
        regex = self.ui_.regularexpressions_list_R.currentItem().text()
        regexinfo = self.results[regex]
        self.ui_.totalitems_R.setText(str(len(regexinfo)))        

    def calculate_distributions(self):
        for regex in self.results:
            data = self.results[regex]
            result = bestdistribution(data)
            self.distributions[regex] = result

    def showerrormessage(self, title, message):
        error_message = QtWidgets.QMessageBox.critical(self.ui_, title, message)

    def show_success_dialog(self):
        success_dialog_signal = QtWidgets.QMessageBox.information(self.ui_, "Tests Successful", 
            "Click on two regular expressions to activate graphing area. Click Help for more information.")
