#!/usr/bin/env
import statistics

from PyQt5 import QtWidgets
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from scipy.stats import norm

class plotcanvaswidget(FigureCanvas):
    def __init__(self, data1, data1name, data2, data2name, parent=None, bins=10, width=10, height=10, dpi=50):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        if len(set(data1)) > 1 and len(set(data2)) > 1:
            self.axes.set_ylabel("Probablity of Occurence")
            self.axes.set_xlabel("GWF")
            minvalue = min(data1 + data2)
            maxvalue = max(data1 + data2)
            bins = np.arange(minvalue, maxvalue, (maxvalue-minvalue)/bins)
            values1, bins1, _ = self.axes.hist(data1, bins=bins, normed=False, alpha=0.5, label=data1name)
            values2, bins2, _ = self.axes.hist(data2, bins=bins, normed=False, alpha=0.5, label=data2name)
            self.plotnormalpdf(data1, values1, bins1, minvalue, maxvalue, (0, 0, 0.5, 0.5))
            self.plotnormalpdf(data2, values2, bins2, minvalue, maxvalue, (0, 1, 0.0, 0.5))
            self.axes.legend(loc='upper right')            
        else:
            self.axes.set_ylabel("Not enough data")
            self.axes.set_xlabel("Not enough data")
        
        FigureCanvas.setSizePolicy(self,
                QtWidgets.QSizePolicy.Preferred,
                QtWidgets.QSizePolicy.Preferred)
        FigureCanvas.updateGeometry(self)

    def plotnormalpdf(self, data, values, bins, minvalue, maxvalue, color):
        mean, stdev = norm.fit(data)
        x = np.linspace(minvalue, maxvalue, 100)
        p = norm.pdf(x, mean, stdev)
        area = sum(np.diff(bins)*values)
        p = [x*area for x in p]
        self.axes.plot(x, p, linewidth=2, color=color)