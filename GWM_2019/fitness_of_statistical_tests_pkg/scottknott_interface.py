#!/usr/bin/env
from collections import namedtuple
import os
from statistics import mean
import sys

import rpy2.robjects as robjects
r = robjects.r

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from fitness_of_statistical_tests_pkg.skpy import skpy

"""
Takes in a list of tuples in the format
[
	(A*B*, [5, 3, 2,1,0]),
	(AB, [1,1,8,3,1])
]
Returns a list of named tuples
Each tuple containing, regex, mean, and rank
Sorted by rank 
"""
def scottknott(regex_list):
	sk_out = namedtuple('sk_out', 'regex mean rank')
	regex_enums = dict()
	for i, regex_gwf in enumerate(regex_list):
		regex_enums[i+1] = regex_gwf[0]

	y = [] 
	xf = []
	for regex_gwf in regex_list:
		for gwf in regex_gwf[1]:
			y.append(gwf)
			xf.append(regex_gwf[0])

	robjects.globalenv['y'] = robjects.FloatVector(y)
	robjects.globalenv['xf'] = robjects.FactorVector(xf)

	r('library(ScottKnott)')
	r('dfm <- data.frame(y,xf)')
	results = None
	sk_result = []
	try:
		results = r('summary(SK(dfm, y=y, model="y ~ xf", which="xf"))')
	except Exception as e:
		results = skpy.scottknott([skpy.Num(x[0], x[1]) for x in regex_list])
		for result in results:
			sk_result.append(sk_out(result.name, mean(result.all), result.rank+1))
	else:
		for i, level in enumerate(results[0]):
			sk_result.append(sk_out(regex_enums[level], results[1][i], results[2][i]))
	
	return sk_result


def main():
	if len(sys.argv) != 2:
		print('One argument of filename')
		sys.exit()

	regex_list = []
	with open(sys.argv[1], 'r') as f:
		data = f.read().splitlines()
		for line in data:
			linedata = line.split()
			regex = linedata[0]
			gwfs = [float(gwf) for gwf in linedata[1:]]
			regex_list.append((regex, gwfs))
	res  = scottknott(regex_list)
	for r in res:
		print(str(r))

if __name__ == '__main__':
	main()