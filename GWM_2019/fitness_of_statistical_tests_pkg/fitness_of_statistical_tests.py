#!/usr/bin/env
import os
import sys

from PyQt5.QtWidgets import QApplication

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from fitness_of_statistical_tests_pkg.fitness_of_statistical_tests_ui import Ui_fitness_of_statistical_tests_widget
from fitness_of_statistical_tests_pkg.fitness_of_statistical_tests_view import fitness_of_statistical_tests_view

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = Ui_fitness_of_statistical_tests_widget()
    view = fitness_of_statistical_tests_view(ui)
    ui.show()
    sys.exit(app.exec_())

