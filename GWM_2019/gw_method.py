#!/usr/bin/env
import os
import sys

from PyQt5.QtWidgets import QApplication

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from encoding_pkg.encoding_ui import Ui_encoding_widget
from encoding_pkg.encoding_view import encoding_view
from synthesizing_pkg.synthesizing_ui import Ui_synthesizing_widget
from synthesizing_pkg.synthesizing_view import synthesizing_view
from fitness_of_statistical_tests_pkg.fitness_of_statistical_tests_ui import Ui_fitness_of_statistical_tests_widget
from fitness_of_statistical_tests_pkg.fitness_of_statistical_tests_view import fitness_of_statistical_tests_view
from categorization_pkg.categorization_ui import Ui_categorization_widget
from categorization_pkg.categorization_view import categorization_view
from gw_method_view import gw_method_view
from gw_method_ui import Ui_gw_method_window

"""
This is the where the program begins. 
Running this script will instantiate all the windows and controls.
Each tab is a different widget and they are independent of one another 
The visual aspect is the _ui files and are auto generated using pyuic5

The _view files do all the controlling
The view files also use other supporting files to complete logic. 

Once each ui has been instantiated, it is passed into the main ui to be the separate tabs. 
"""
if __name__ == '__main__':
    app = QApplication(sys.argv)

    # instantiate widget for each tab
    encoding_widget = Ui_encoding_widget()
    synthesizing_widget = Ui_synthesizing_widget()
    fitness_of_statistical_tests_widget = Ui_fitness_of_statistical_tests_widget()
    categorization_widget = Ui_categorization_widget()\
    # create main window
    ui = Ui_gw_method_window(fitness_of_statistical_tests_widget, encoding_widget, synthesizing_widget, categorization_widget)

    # instantiate views to controls widgets in each tab
    encoding_view = encoding_view(encoding_widget)
    synthesizing_view = synthesizing_view(synthesizing_widget)
    fitness_of_statistical_tests_view = fitness_of_statistical_tests_view(fitness_of_statistical_tests_widget)
    categorization_view = categorization_view(categorization_widget)
    # instnatiate main view to control help button
    gw_method_view = gw_method_view(ui, encoding_view, categorization_view, synthesizing_view, fitness_of_statistical_tests_view)

    # display the ui
    ui.showMaximized()
    sys.exit(app.exec_())

# If you ever wonder yourself asking why I did something the way I did. The answer could be long and tired nights. I'm sorry.