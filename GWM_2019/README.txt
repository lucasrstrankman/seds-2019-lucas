﻿For Developers and students extending the code:

For installation, follow the steps in the User Manual under Reports. 

This program was written on Python 3.4.3 using PyQt5
Last modified on August 31, 2015. 

Use gitk to check revision history. 

There are 3 important branches. Master, obfuscated_distribution, distribution.

Master is where you should develop on, the reports are updated and you should see if there's information
in there if you ever get confused. Algorithms are documented and up to date for Encoding, Categorization, and Merging.
Each tab is organized into the *_pkg folders and can be run as a standalone using 
encoding.py, categorization.py, synthesizing.py, or fitness_of_statistical_tests.py 
The .ui files can be opened with Qt Designer and edited there. To be used in the program, they must be converted to a .py file.
To convert a .ui to a .py file, use the command pyuic5 inputfile -o outputfile, for clarity all files converted from .ui have the 
prefex _ui.py. The only editing of the _ui.py files should be changing the class to inherit QtWidgets. The ui logic should belong
under _view.py files, due to lack of foresight from me, there is some backend logic in some of the _view.py files, but if you want to improve the
code you should change this to only handle ui elements. 

distribution has been compiled into an exe under dist/gw_method. Just click gw_method.exe to open.
There are steps to install R first in order for the program to run correctly, otherwise you will
get a fatal error that gw_method has returned -1. The command used to compile the program is
pyinstaller gw_method.py --hidden-import=scipy.stats.scipy.linalg.cython_blas --hidden-import=scipy.inalg.cython-lapack --noconsole
After this, the files from gwdist need to copied into the dist/gw_method/ in order for the program to run.
For simpler compilation, everything was moved into the root rather than organized into separate directories i.e. encoding_pkg
Thus some of the import have changed from master.
Statsmodels also would not compile due to some problem with pandas. So I copied pasted the code I needed from statsmodels and didn't import it.
This is kind of disguisting but it works, you can see it in fitness_of_statistical_tests_view.py.\
This branch is meant to be distributed to SEDS lab members.

Obfuscated distribution is the same code as master, but the code meant to be unreadable. 
I have changed all the variable, function, class, and file names to gibberish. 
I also increased white spacing inside () and beside certain operators like =, ==, or + but that is not consistent.
gw_method.py should still be run and have the same results. Alot of files have been removed because this branch
is meant to be distributed to other researchers but Maleknaz wanted to keep the logic of the code secret. 



 
