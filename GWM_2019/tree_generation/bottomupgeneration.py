import itertools

regexchildgraph = {}
regexparentgraph = {}
regexdistancegraph = {}


def generate_events(numberofevents):
    if numberofevents <= 0 or numberofevents > 26:
        return []
    startingletter = 'A'
    event_types = []
    for i in range(numberofevents):
        event_types.append(chr(ord(startingletter) + i))
    return event_types


def generate_eventpermutations(events):
    permutations = []
    for i in range(len(events)):
        permutations += \
            list(map("".join, itertools.permutations(events, i + 1)))
    return permutations


def event_typescount(event):
    return len(event.replace('*', '').replace('(', '').replace(')', ''))


def findnonstarindices(event):
    nonstarindices = []
    index = 0
    for char in event:
        nonstarindices.append(index)
        if char == '*':
            nonstarindices.remove(index)
            if index-1 in nonstarindices:
                nonstarindices.remove(index-1)
        index += 1
    if event[0] is '(':
        nonstarindices.remove(0)
    return nonstarindices


def addstartoeventpermutations(eventpermutations, star_rank=1):
    staredpermutations = []
    for event in eventpermutations:
        if event_typescount(event) == 1 and event.count('*') == 0:
            staredpermutations.append(event + '*')
        else:
            tostarindices = findnonstarindices(event)
            for tostarindex in tostarindices:
                result = ''
                iterator = 0
                while iterator < len(event):
                    result += event[iterator]
                    if iterator is tostarindex:
                        result += '*'
                    iterator += 1
                staredpermutations.append(result)
    return staredpermutations


def connectregularexpressions(parent, child):
    global regexchildgraph
    global regexparentgraph
    if parent in regexchildgraph:
        regexchildgraph[parent].add(child)
    else:
        regexchildgraph[parent] = {child}

    if child in regexparentgraph:
        regexparentgraph[child].add(parent)
    else:
        regexparentgraph[child] = {parent}


def skipstarletters(regex):
    nonstarindices = findnonstarindices(regex)
    result = ''
    for index in nonstarindices:
        result += regex[index]
    return result


def compareregularexpressions(childset, parentset):
    parentswithoutchildren = False
    for parent in parentset:
        parenthaschild = False
        for child in childset:
            if checkforconnectforadditionalstars(parent, child) \
                    or checkforconnectskippingstar(parent, child):
                connectregularexpressions(parent, child)
                parenthaschild = True
        if not parenthaschild:
            parentswithoutchildren += True

    return parentswithoutchildren

def comparematchingletters(childset, parentset):
    parentswithoutchildren = False
    for parent in parentset:
        parenthaschild = False
        for child in childset:
            if checkifsameletters(parent, child):
                connectregularexpressions(parent, child)
                parenthaschild = True
        if not parenthaschild:
            parentswithoutchildren += True

    return parentswithoutchildren

def checkifsameletters(parent, child):
    parentletters = set(parent.replace('*','').replace('(','').replace(')',''))
    childletters = set(child.replace('*','').replace('(','').replace(')',''))
    return parentletters.intersection(childletters) == childletters


def checkforconnectforadditionalstars(parent, child):
    if parent.replace('*', '').replace('(', '').replace(')', '') != \
            child.replace('*', '').replace('(', '').replace(')', ''):
        return False
    if '(' in parent:
        if '(' in child:
            if parent.count('*') - child.count('*') == 1:
                return len(letterswithstars(parent[1:-2]).difference(letterswithstars(child[1:-2]))) == 1 
            else:
                return False
        else:
            if parent.count('*') == 1:
                return parent[1:-2] == child
            else:
                return parent[1:-2] == child
    else:
        if '(' in child:
            return False
        else:
            if parent.count('*') - child.count('*') == 1:
                return len(letterswithstars(parent).difference(letterswithstars(child))) == 1
            else:
                return False

def letterswithstars(regex):
    result = set()
    i = 0
    while i < len(regex):
        try:
            if regex[i+1] == '*':
                result.add(regex[i])
                i += 2
            else:
                i += 1
        except IndexError:
            break
    return result


def checkforconnectskippingstar(one, two):
    onestarindices = [i for i, letter in enumerate(one) if letter == '*']
    twostarindices = [i for i, letter in enumerate(two) if letter == '*']
    for indice in onestarindices:
        if one[:indice-1] + one[indice+1:] == two:
            return True
    for indice in twostarindices:
        if two[:indice-1] + two[indice+1:] == one:
            return True
    return False


def encloseregularexpressions(permutations):
    enclosed = []
    for perm in permutations:
        if event_typescount(perm) != 1:
            enclosed.append('(' + perm + ')*')
    return enclosed


def generate_distancegraph(childeventgraph, numberofevents):
    roots = []
    distancegraph = {}
    hops = 0
    for regex in childeventgraph:
        if regex.count('*') == numberofevents + 1:
            roots.append(regex)
    for root in roots:
        distancegraph[root] = 0
    queue = {roots[0]}
    while len(queue) != 0:
        for regex in queue.copy():
            distancegraph[regex] = hops
        for parent in queue.copy():
            if parent in childeventgraph:
                for child in childeventgraph[parent]:
                    if child not in distancegraph:
                        queue.add(child)
            queue.remove(parent)
        hops += 1
    return distancegraph

def main():
    numberofevents = int(input('Enter the number of events:\n'))
    outputfile = open("bottomupresults" + str(numberofevents) + ".txt", 'w')
    event_types = generate_events(numberofevents)
    seta = generate_eventpermutations(event_types)
    setd = []
    relationsremaining = False  # track if no child was assigned any regex
    star = 0
    while star <= numberofevents:
        setb = addstartoeventpermutations(seta)
        relationsremaining = compareregularexpressions(seta, setb)
        setc = encloseregularexpressions(seta)
        if setd is not []:
            root_setc = set()
            nonroot_setc = set()
            for regex in setc:
                if event_typescount(regex)+1 == regex.count('*'):
                    root_setc.add(regex)
                else:
                    nonroot_setc.add(regex)
            relationsremaining = comparematchingletters(setd+seta, root_setc)
            relationsremaining += compareregularexpressions(setd, nonroot_setc)
        if relationsremaining:
            compareregularexpressions(seta, setc)
        seta = setb
        setd = setc
        star += 1

    # print('regexchildgraph is ')
    # for child in regexchildgraph:
    #     print(child + ' : ' + str(regexchildgraph[child]))

    regexdistancegraph = generate_distancegraph(regexchildgraph, numberofevents)

    for key, value in sorted(regexchildgraph.copy().items()):
        outputfile.write(str(key) + " " +
                         str(regexdistancegraph[key]) + "\n")
        outputfile.write("      children:" +
                         str(regexchildgraph[key]) + "\n")
        if key in regexparentgraph:
            outputfile.write("      parents:" +
                             str(regexparentgraph[key]) + "\n")
    for key, value in sorted(regexparentgraph.copy().items()):
        if key.count("*") == 0:
            outputfile.write(str(key) + " " +
                             str(regexdistancegraph[key]) + "\n")
            outputfile.write("      parents: " +
                             str(regexparentgraph[key]) + "\n")

    outputfile.write(str(len(set(regexchildgraph.keys()).union(set(regexparentgraph.keys())))) +
                             " items in this graph ")
    print(str(len(set(regexchildgraph.keys()).union(set(regexparentgraph.keys())))) + " items in this graph ")

if __name__ == '__main__':
    main()
