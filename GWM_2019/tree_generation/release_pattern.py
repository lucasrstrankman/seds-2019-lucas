class releasePattern:
	"""
	Ex. event_pattern() = "A*(BC)*"
		event_order_ = "ABC"
		parentheses_ = [1,2]
		stars_ = [True, False, False]

	Ex. event_pattern() = "(B*A*)*(D*C)*"
		event_order_ = "BADC"
		parentheses_ = [2,2]
		stars_ = [True, True, True, False]

	Ex. event_pattern() = "AB*DC"
		event_order_ = "ABDC"
		parentheses_ = [1,1,1,1]
		stars_ = [False, True, False, False]
	"""
	def __init__(self, event_order, parentheses, stars):
		self.event_order_ = event_order
		self.parentheses_ = parentheses
		self.stars_ = stars

	def event_pattern(self):
		output = ""
		event_index = 0
		for bracket_length in self.parentheses_:
			if bracket_length == 1:
				output += self.next_letter(event_index)
				event_index += 1
			else:
				output += '(' 
				i = 0
				for i in range(bracket_length):
					output += self.next_letter(event_index)
					event_index += 1
				output += ')*'
		return output

	def next_letter(self, event_index):
		print('event_order_ is ' + str(event_order))
		if self.stars_[event_index]:
			return self.event_order_[event_index] + '*'
		else:
			return self.event_order_[event_index]
		