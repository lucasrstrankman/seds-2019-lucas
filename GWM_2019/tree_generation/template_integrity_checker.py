#!/usr/bin/env
from itertools import permutations
import sys

class template_integrity_checker():
    def __init__(self):
        self.regex_template = dict()
        self.size = None

    def load_graph(self, filename, size):
        self.__init__()
        self.size = int(size)
        with open(filename, 'r') as datafile:
            data = datafile.read().splitlines()
            activeregex = None
            for line in data[:-1]:
                if 'children' in line:
                    regexes = line[17:-1].replace("'","").replace(",", "").split()
                    self.regex_template[activeregex].children += regexes
                elif 'parents' in line:
                    regexes = line[16:-1].replace("'","").replace(",", "").split()
                    self.regex_template[activeregex].parents += regexes
                else:
                    activeregex = line.split()[0]
                    # print('activeregex is ' + activeregex)
                    self.regex_template[activeregex] = Node(activeregex)
        return self.regex_template

    """
    Either raises an error
    or returns None
    """
    def check_integrity(self):
        if not self.regex_template:
            raise ValueError('Unitialized Regex Template')
        try:
            print('checking parent child relationships')
            self.check_parent_child_relationships()
            print('checking leafs childless')
            self.check_leafs_childless()
            print('checking root parentless')
            self.check_root_parentless()
            print('checking root child no parentheses')
            self.check_root_child_no_parentheses()
            print('checking root permutations')
            self.check_root_permutations()
        except Exception as e:
            raise(e)
        else:
            return True

    """
    Checks that for every relationship where regex A is a parent of regex B, 
    then regex B is a child of regex A
    """
    def check_parent_child_relationships(self):
        for regex in self.regex_template:
            for parent in self.regex_template[regex].parents:
                if regex not in self.regex_template[parent].children:
                    raise ValueError('Parent child relationship not mutual between ' + 
                        parent + ' and ' + regex)
        for regex in self.regex_template:
            for child in self.regex_template[regex].children:
                if regex not in self.regex_template[child].parents:
                    raise ValueError('Parent child relationship not mutual between ' + 
                        child + ' and ' + regex)


    """
    For regexes that have the same stars, there are no duplicates via
    permutations. Such as if (A*B*)* exists, then (B*A*)* ghdoes not exist.
    Further, the one regex of that permutation that exists, should be the 
    minimum one lexicographically.

    EXAMPLE
    (B*C*D*)* exists, 
    so (B*D*C*)*, (C*B*D*)*, (D*B*C*)*, (C*D*B*)*, (D*C*B*)* should not exist
    """
    def check_root_permutations(self):
        roots_letters = []
        for regex in self.regex_template:
            if regex.count('*')-1 == len(self.regex_template[regex].letters): 
                roots_letters.append(''.join(self.regex_template[regex].letters))
        if len(set(roots_letters)) != len(roots_letters):
            for root in set(roots_letters.copy()):
                roots_letters.remove(root)
            raise ValueError('Duplicate roots for letters ' + str(set(roots_letters)))


    """
    Check that all regexes which have no stars have no children
    """
    def check_leafs_childless(self):
        for regex in self.regex_template:
            if regex.count('*') == 0:
                if self.regex_template[regex].children:
                    raise ValueError(regex + ' should have no chlidren but has ' +
                        str(self.regex_template[regex].children)) 

    """
    Check that the root regex has no parents
    """
    def check_root_parentless(self):
        for regex in self.regex_template:
            if regex.count('*')-1 == self.size:
                if self.regex_template[regex].parents:
                    raise ValueError(regex + ' should have no parents but has ' + 
                        str(self.regex_template[regex].parents))

    """
    Check that all regexes that have the maximum amount of stars
    Have the sames regexes but without parentheses as children. All permutations
    
    EXAMPLE
    (B*C*)*'s children must include B*C* and C*B*
    (B*C*D*)*'s children must include B*C*D*, B*D*C*, C*B*D*, D*B*C*, C*D*B*, D*C*B*
    """
    def check_root_child_no_parentheses(self):
        for regex in self.regex_template:
            if regex.count('*')-1 == len(self.regex_template[regex].letters): 
                letter_perms = [''.join(p) for p in permutations(self.regex_template[regex].letters)]
                children = []
                for letter_perm in letter_perms:
                    child = ''.join([x+'*' for x in letter_perm])
                    children.append(child)
                if not set(children).issubset(set(self.regex_template[regex].children)):
                    raise ValueError(regex + ' missing children from ' + str(children))

class Node():
    def __init__(self, regex):
        self.regex = regex
        self.children = []
        self.parents = []
        self.letters = self.regex_letters(regex)

    def __repr__(self):
        representation = '\nREGEX: ' + self.regex + '\n'
        representation += '     CHILDREN: ' + str(self.children) + '\n'
        representation += '     PARENTS: ' + str(self.parents) + '\n'
        return representation

    def regex_letters(self, regex):
        return sorted(regex.replace("*","").replace(")","").replace("(",""))

def main():
    print('Intializing')
    checker = template_integrity_checker()
    if len(sys.argv) != 3:
        print('2 Arguments [template filename, template size]')
        sys.exit()
    print('Loading')
    checker.load_graph(sys.argv[1], int(sys.argv[2]))
    if checker.check_integrity():
        print("\n***PASS***")
    else:
        print("\n***FAIL***")


if __name__ == '__main__':
    main()