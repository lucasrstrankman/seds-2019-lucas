import sys

"""
This class takes in an already created regex template file by bottomupgeneration.py
It then ensures that every root is all merged together
So it ensures things like (A*B*)* and (B*A*)* don't exist in the same graph because they are the same regex
But (A*B*C*)* and (A*C*)* can both still exist
If there are duplicates like listed above, it merges all the parent and child relationships together

This is done using the self.fix_graph() method
But using the filename, a graph must be loaded into a root_template_fixer object first 
using the load_graph() method
"""
class root_template_fixer():
    def __init__(self):
        self.regex_template = dict()

    def load_graph(self, filename):
        self.__init__()
        with open(filename, 'r') as datafile:
            data = datafile.read().splitlines()
            activeregex = None
            for line in data[:-1]:
                if 'children' in line:
                    regexes = line[17:-1].replace("'","").replace(",", "").split()
                    self.regex_template[activeregex].children += regexes
                elif 'parents' in line:
                    regexes = line[16:-1].replace("'","").replace(",", "").split()
                    self.regex_template[activeregex].parents += regexes
                else:
                    activeregex = line.split()[0]
                    self.regex_template[activeregex] = Node(activeregex, line.split()[1])
        return self.regex_template

    def output_graph(self, outputfile):
        with open(outputfile, 'w') as outputfile:
            for regex in sorted(self.regex_template.keys()):
                outputfile.write(str(regex) + ' ' + str(self.regex_template[regex].level) + '\n')
                if self.regex_template[regex].children:
                    outputfile.write("      children:" + str(set(self.regex_template[regex].children)) + '\n')
                if self.regex_template[regex].parents:
                    outputfile.write("      parents:" + str(set(self.regex_template[regex].parents)) + '\n')
            outputfile.write(str(len(self.regex_template.keys())) + ' items in this graph')

    def fix_graph(self):
        root_groups = dict()    
        for regex in self.regex_template:
            if self.is_root(regex):
                try:
                    root_groups["".join(self.regex_template[regex].letters)].append(regex)
                except KeyError:
                    root_groups["".join(self.regex_template[regex].letters)] = [regex]
        print('root_groups are ' + str(root_groups.keys()))
        
        doomed_regexes = set()
        
        for group in root_groups:
            group_parents = []
            group_children = []
            true_root = min(root_groups[group])
            
            for regex in root_groups[group]:
                doomed_regexes.add(regex)
                group_parents += self.regex_template[regex].parents
                group_children += self.regex_template[regex].children
            
            self.regex_template[true_root].parents = list(set(group_parents))
            doomed_regexes.remove(true_root)
            self.regex_template[true_root].children = list(set(group_children))

        for regex in self.regex_template.copy():
            if regex in doomed_regexes:
                self.regex_template.pop(regex)
            else:
                for child in self.regex_template[regex].children.copy():
                    if child in doomed_regexes:
                        self.regex_template[regex].children.remove(child)
                for parent in self.regex_template[regex].parents.copy():
                    if parent in doomed_regexes:
                        self.regex_template[regex].parents.remove(parent)

        return self.regex_template



    def is_root(self, regex):
        if regex.count('*') == len(self.regex_template[regex].letters) + 1:
            return True
        else:
            return False


class Node():
    def __init__(self, regex, level):
        self.regex = regex
        self.children = []
        self.parents = []
        self.level = level
        self.letters = self.regex_letters(regex)

    def __repr__(self):
        representation = '\nREGEX: ' + self.regex + ' ' + str(self.level) + '\n'
        representation += '     CHILDREN: ' + str(self.children) + '\n'
        representation += '     PARENTS: ' + str(self.parents) + '\n'
        return representation

    def regex_letters(self, regex):
        return sorted(regex.replace("*","").replace(")","").replace("(",""))

def main():
    if len(sys.argv) != 3:
        print('The 2 arguments are [inputfile outputfile]')
        sys.exit()
    rtf = root_template_fixer()
    print('loading graph')
    rtf.load_graph(sys.argv[1])
    print('fixing graph')
    rtf.fix_graph()
    print('printing graph')
    rtf.output_graph(sys.argv[2])

if __name__ == '__main__':
    main()