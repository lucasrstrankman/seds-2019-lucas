import csv

def convert(path):
    info = []
    output = ""
    with open(path, 'rt') as f:
        reader = csv.reader(f)
        info = list(reader)

    for line in info:
        output += line[1] + " "
        output += line[2] + "\n"


    with open("encodingOutput.txt", 'w') as f:  # The [-4] removes the .csv at the end
        f.write(output)

def main():
    # print("Enter a file name")
    # fileName = input()
    fileName = "2020-03-06_11-07-16_RQ1 output_log.csv"
    convert(fileName)
    
if __name__ == "__main__":
    main()