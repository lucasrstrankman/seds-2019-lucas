# for each app
    # for each implemented feature
        # find the first review mentioning it
            # imp-date - firstreview-date is quality
        # create weeks from the first mention to imp date
        # Each review is put into one of those weeks
        
        # For week in weeks
            # if week is > prev: A
            # if week is = prev: B
            # if week is < prev: C
        # add this to patterns

from utilities import *
from datetime import date
from datetime import timedelta
import math

featurePath = 'feature_dates/'
reviewPath = 'feature_groups/'

FEATURE_STARTS_WITH = "featureImpDates_"
REVIEW_STARTS_WITH = "groups_"


def convertCommitDate(day): # Because the commits and the reviews have their dates stored differently
    d1 = day.split('/')
    if len(d1[1]) == 1:
        d1[1] = "0" + d1[1]
    if len(d1[2]) == 1:
        d1[2] = "0" + d1[2]
    d = d1[0] + "-" + d1[1] + "-" + d1[2]
    return d


def extractFeatures(path):
    info = read_csv(path)
    features = []
    for line in info:
        if (len(line) > 0):
            features.append( (line[0] , convertCommitDate(line[1])) )
    return features


def extractReviews(path):
    info = []
    with open(path, 'rt', errors='ignore') as f:
        reader = csv.reader(f)
        info = list(reader)   
    reviews = {}
    for line in info[1:]:
        if (len(line) > 0):
            if line[0] in info:
                dates = reviews[line[0]]
                print(line[3])
                reviews[line[0]] = dates.append(line[3])
            else:
                reviews[line[0]] = [line[3]]
    for feat in reviews:
        reviews[feat].sort()
    return reviews


def putReviewsIntoWeeks(rdates, firstDate, lastDate):
    weeks = []
    fday = date.fromisoformat(firstDate)
    lday = date.fromisoformat(lastDate)
    day = date.fromisoformat(firstDate)


    while day < lday:
        weeks.append((day, 0))
        day = day + timedelta(days=7)
    

    print(len(rdates))
    for week in weeks:
        for rdate in rdates:
            d = date.fromisoformat(rdate)
            wday, count = week
            if d >= wday:
                count += 1
                week = (wday, count)
                break
    return weeks


def main():
    allPatterns = []
    featureApps = os.listdir(featurePath)
    before = 0
    after = 0
    count = 0
    rcount = 0
    for csv in featureApps:
        app = csv[len(FEATURE_STARTS_WITH):]

        features = extractFeatures(featurePath + FEATURE_STARTS_WITH + app)

        # Is a dictionary from feat -> array of dates
        reviews = extractReviews(reviewPath + REVIEW_STARTS_WITH + app)
        count += len(features)
        rcount += len(reviews)
        print("len of features {0}".format(len(features)))
        for feature in features:
            feat, fdate = feature
            if feat in reviews:
                rdates = reviews[feat]
                firstMentionDate = date.fromisoformat(rdates[0])
                implementDate = date.fromisoformat(fdate)
                
                if (firstMentionDate <= implementDate):

                    before += 1
                else:
                    after += 1
    print()
    print("# of Commits Features: %d" % count)
    print("# of Review Features: %d" % rcount)
    print("# of Reviews before commit: %d" % before)
    print("# of Reviews after commit: %d" %after)


if __name__ == '__main__':
    print("starting")
    main()