# One sequence per release and one item per week
# Count the number of bug review per week and categorize that week as either:
# HIGH: -> Week has more bug reviews than 75% of all weeks across releases of that app
# Mid: If the given week has between more than 25% and fewer than 75%
# Low: Fewer bug reports than 25% of all weeks

# Performance measure is ratio of bug commits in the given release 
# to avg. num of bug comits over all releases for that app


from utilities import *
import csv
from datetime import date
from datetime import timedelta
import math


STARTS_WITH = "intersection"
FILE_NAME_LENGTH = len(STARTS_WITH)
TYPE_OF_REVIEW = "b"
NUM_OF_DAYS = 21
LOW_CUT_OFF = 0.25
HIGH_CUT_OFF = 0.75

PATH = 'Apps/'
FOLDER = "/" + STARTS_WITH + "/"

# #
def extractReviews(appPath):
    files = os.listdir(appPath)
    totalReviews = 0
    releases = []
    releaseReviewCount = {}

    for csvFile in files:
        if csvFile.startswith(STARTS_WITH + "_lemmatized_reviews_"):
            version = csvFile[(FILE_NAME_LENGTH+20):-4]
            reviews = []
            info = []
            with open(appPath + csvFile, 'rt', errors='ignore') as f:
                reader = csv.reader(f)
                info = list(reader)
            for line in info:
                if (len(line) > 0) and line[1] == TYPE_OF_REVIEW:
                    line.append(version)
                    reviews.append(line)
                    totalReviews += 1

            releases.append(reviews)
            releaseReviewCount[version] = ((len(reviews),0,""))  # count the number of bug reviews in the version

    averageNumReviews = totalReviews/len(releaseReviewCount)
    if (averageNumReviews > 0):
        for key in releaseReviewCount:
            numOfReviews,_,_ = releaseReviewCount[key]
            releaseReviewCount[key] = (numOfReviews, numOfReviews/averageNumReviews,"")
    return releases,releaseReviewCount

def extractCommits(appPath):
    files = os.listdir(appPath)
    totalCommits = 0
    numOfReleases = 0
    commits = {}
    for csv in files:
        if csv.startswith(STARTS_WITH + "_lemmatized_commits_"):
            numOfReleases += 1
            releaseCommits = 0

            version = csv[(FILE_NAME_LENGTH + 20):-4]
            info = read_csv(appPath + csv)
            for line in info:
                if (len(line) > 0) and line[1] == TYPE_OF_REVIEW:
                    releaseCommits += 1
            commits[version] = releaseCommits
            totalCommits += releaseCommits
    
    appAverageCommits = totalCommits/numOfReleases
    for version in commits:
        count = commits[version]
        if appAverageCommits > 0:
          commits[version] = count/appAverageCommits
        else:
            commits[version] = 0

    return commits

def putReviewsIntoWeeks(release):
    release.sort(key = lambda x: x[2]) # Sort by date
    if (release):
        firstDate = date.fromisoformat(release[0][2])
        lastDate = date.fromisoformat(release[-1][2])
        span = lastDate - firstDate

        if (firstDate == lastDate): # If there is only one review in that release of the Type
            weeks = [(0,0,"","")]
        else:
          weeks = [(0,0,"","")] * math.ceil(span.days/NUM_OF_DAYS)  # ceil because is there a decimal it needs its own week

        if (len(weeks) > 0):
            for _,_, isoDate,_,_,version in release:
                bugDate = date.fromisoformat(isoDate)
                week = math.floor((bugDate - firstDate).days/NUM_OF_DAYS) # floor because the list is already 0-indexed
                if (week >= len(weeks)) and week != 0:
                    week -= 1
                _,count,_,_ = weeks[week]
                weeks[week] = (week, count+1,version,"")
            return weeks
        else:
            return []
    else:
        return []


# Use to remove all the empty weeks with no reviews
def cleanWeeks(weeks):
    cleanedWeeks = []
    for week in weeks:
        _,count,_,_ = week
        if count > 0:      
            cleanedWeeks.append(week)
    return cleanedWeeks


# Orders the weeks by number of reviews and ranks them based on their position
def assignCode(weeks):
    weeks.sort(key = lambda x: x[1])
    numOfWeeks = len(weeks)

    low = math.floor(numOfWeeks * LOW_CUT_OFF)
    high = math.ceil(numOfWeeks * HIGH_CUT_OFF)

    for i in range(0,low):
        week,count,version,_ = weeks[i]
        weeks[i] = (week, count, version, "L ")
    for i in range(low,high):
        week,count,version,_ = weeks[i]
        weeks[i] = (week, count, version, "M ")
    for i in range(high,numOfWeeks):
        week,count,version,_ = weeks[i]
        weeks[i] = (week, count, version, "H ")
    weeks.sort(key = lambda x: x[0])


def createPatterns(weeks, releaseReviewCount, commits):
    for week in weeks:
        _,_,version,code = week
        numOfReviews, _, pattern = releaseReviewCount[version]
        perfMeasure = commits[version]
        
        releaseReviewCount[version] = (numOfReviews, perfMeasure, pattern + code)

    patterns = []
    for version in releaseReviewCount:
        
        numOfReviews,performanceMeasure,pattern = releaseReviewCount[version]
        if numOfReviews > 0:
            patterns.append((pattern, performanceMeasure))
    return patterns



def main():
    allPatterns = []
    apps = os.listdir(PATH)
    for app in apps:
        folderPath = PATH + app + FOLDER
    
        if (os.path.isdir(folderPath)): # If the director with the reviews exists, not all apps have it
            releases, releaseReviewCount = extractReviews(folderPath) 
            commits = extractCommits(folderPath)
            allWeeks = []

            # Create a weeks for each release separately
            for release in releases:
                weeks = putReviewsIntoWeeks(release)
                weeks = cleanWeeks(weeks)       # Removes the empty weeks with no reviews
                for week in weeks:
                    allWeeks.append(week)
            
            assignCode(allWeeks)               # Assign the codes to the weeks
            patterns = createPatterns(allWeeks, releaseReviewCount, commits)
            
            for pattern in patterns:
                allPatterns.append(pattern)
    for pattern in allPatterns:
        print(pattern)
    write_csv('RQ1.csv', allPatterns)


if __name__ == '__main__':
    print("starting")
    main()