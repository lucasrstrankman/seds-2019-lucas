import os
import csv
from distutils.version import StrictVersion
from string import ascii_lowercase

def check_path(path):
    if not os.path.exists(path):
        os.mkdir(path)


def check_paths(*paths):
    for path in paths:
        check_path(path)


def read_csv(path):
    info = []
    with open(path, 'rt') as f:
        reader = csv.reader(f)
        info = list(reader)
    return info


def write_csv(path, info):
    with open(path, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(info)


def sort_versions(vs):
    versions = []
    for file in vs:
        safe_version = file
        safe_version = safe_version.lower()
        safe_version = safe_version.replace('_', '')
        safe_version = safe_version.replace('-', '')
        for letter in ascii_lowercase:
            safe_version = safe_version.replace(letter, '')
        if safe_version.startswith('.'):
            safe_version = safe_version[1:]
        if safe_version.endswith('.'):
            safe_version = safe_version[:-1]
        if safe_version.count('.') > 2:
            safe_version = safe_version[::-1].replace('.', '', 1)[::-1]
        if len(safe_version) == 0:
            safe_version = '1.0'
        elif len(safe_version) == 1:
            safe_version += '.0'
        try:
            StrictVersion(safe_version)
        except ValueError:
            return sorted(vs)

        versions.append((safe_version, file))

    versions_sorted = sorted(versions, key=lambda x: StrictVersion(x[0]))
    return [version for _, version in versions_sorted]


def get_topic_suffixes():
    return ['', '_jaccard', '_weighted']
