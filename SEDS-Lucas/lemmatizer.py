from utilities import *
import re
import os
from nltk.tokenize import sent_tokenize
from nltk.stem import WordNetLemmatizer
import enchant

lemmatizer = WordNetLemmatizer()
ench = enchant.Dict('en_US')

def to_unicode(str):
    return ''.join([x if ord(x) < 128 else '' for x in str]).strip()


def remove_brackets(sentence):
    new_sent = ""
    bracket_count = 0
    for i, ch in enumerate(sentence):
        if ch == '(':
            bracket_count += 1
        elif ch == ')':
            bracket_count -= 1
        else:
            if bracket_count == 0:
                new_sent += ch
    return new_sent


def contains(sentence, characters):
    for char in characters:
        if char in sentence:
            return True
    return False


def filter_sentences(sentences):
    filtered = []
    filters = ['\'', '\"', 'www', '.com', '@', 'http', 'https']
    for i, sent in enumerate(sentences):
        sent = remove_brackets(sent)
        if not contains(sent, filters):
            filtered.append(sent)
    return filtered


def remove_subordinates(sentences):
    # https://www.grammarly.com/blog/subordinating-conjunctions/
    subordinates = ['after', 'although', 'as', 'as if', 'as long as', 'as much as',
    'as soon as', 'as though', 'because', 'before', 'by the time', 'even if',
    'even though', 'if', 'in order that', 'in case', 'in the event that', 'lest',
    'now that', 'once', 'only', 'only if', 'provided that', 'since', 'so',
    'supposing', 'that', 'than', 'though', 'till', 'unless', 'until', 'when',
    'whenever', 'where', 'whereas', 'wherever', 'whether or not', 'while']
    for i, sent in enumerate(sentences):
        sent = sent.lower().strip()
        sentences[i] = sent
        for sub in subordinates:
            splitted = sent.split(' ')
            if sub in splitted:
                idx = sent.find(sub)
                sentences[i] = (sent[:idx].strip() + ' ' + sent[idx + len(sub) + 1:].strip()).strip()
    cleaned = []
    for sent in sentences:
        if len(sent) > 0:
            cleaned.append(sent)
    return cleaned


def copy_insert(arr, val, idx):
    new = [v for v in arr]
    new[idx] = val
    return new


def remove_repeats(arr):
    new = []
    for val in arr:
        if len(new) == 0:
            new.append(val)
        elif new[-1] != val:
            new.append(val)
    return new


def lemmatize(app, data_type, data_idx, filter_func = lambda x : False):
    path = "Apps/%s/" % (app)
    library = os.listdir(path)

    total_reviews = 0
    num_skipped = 0
    num_sentences = 0

    target_reg = re.compile(r"%s_(.+)" % data_type)

    check_path(path + '/lemmatized/')

    for file in library:
        match = target_reg.match(file)
        if match:
            info = read_csv(path + file)

            final = [info[0]]
            for line in info[1:]:
                total_reviews += 1
                if filter_func(line):
                    num_skipped += 1
                    continue
                data = to_unicode(line[data_idx])
                sentences = sent_tokenize(data)
                filtered = filter_sentences(sentences)
                cleaned = remove_subordinates(filtered)
                for r in [c.lower().translate(None, '.?!@#$%^&*-+:') for c in cleaned]:
                    r = re.sub(' +', ' ', r).strip()
                    if len(r) > 0:
                        r = r.split(' ')
                        r = [word for word in r if ench.check(word)]
                        r = list(map(lemmatizer.lemmatize, r))
                        r = remove_repeats(r)
                        r = ' '.join(r)
                        nl = copy_insert(line, r, data_idx)
                        final.append(nl)
                        num_sentences += 1

            write_csv('Apps/%s/lemmatized/lemmatized_%s' % (app, file), final)
