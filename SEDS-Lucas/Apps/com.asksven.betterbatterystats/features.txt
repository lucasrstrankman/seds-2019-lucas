analyze phone behavior
find applications causing battery drain
measure corrective actions
detect changes in awake profile
detect changes in sleep profile
find causes of profile changes