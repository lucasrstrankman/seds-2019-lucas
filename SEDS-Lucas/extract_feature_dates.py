# Code adapted from 1.1.create_matrix.py


from create_matrix import *
import sys
import pattern.vector
from pattern.vector import Document, NB, SVM
from pattern.en import parse
import os
import csv
import re
import string
import math
import nltk
# nltk.download()
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import *
import enchant
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
import random
from collections import defaultdict
from utilities import *

lemmatizer = WordNetLemmatizer()
stemmer = PorterStemmer()

global_stats = defaultdict(int)
app_stats = {}
apps_skipped = 0
apps_analyzed = []

extra_stops = []
with open('stop_words', 'r') as f:
    lines = f.readlines()
    extra_stops = [l.strip() for l in lines]
STOP_WORDS = stopwords.words('english') + extra_stops
global_stats["0) Number of Stop Words"] = len(STOP_WORDS)

ench = enchant.Dict('en_US')

def to_unicode(str):
    return ''.join([x if ord(x) < 128 else '' for x in str]).strip()


def remove_brackets(sentence):
    new_sent = ""
    bracket_count = 0
    for i, ch in enumerate(sentence):
        if ch == '(':
            bracket_count += 1
        elif ch == ')':
            bracket_count -= 1
        else:
            if bracket_count == 0:
                new_sent += ch
    return new_sent


def contains(sentence, characters):
    for char in characters:
        if char in sentence:
            return True
    return False


def filter_sentences(sentences):
    filtered = []
    filters = ['\'', '\"', 'www', '.com', '@', 'http', 'https']
    for i, sent in enumerate(sentences):
        sent = remove_brackets(sent)
        if not contains(sent, filters):
            filtered.append(sent)
    return filtered


def remove_subordinates(sentences):
    # https://www.grammarly.com/blog/subordinating-conjunctions/
    subordinates = ['after', 'although', 'as', 'as if', 'as long as', 'as much as',
    'as soon as', 'as though', 'because', 'before', 'by the time', 'even if',
    'even though', 'if', 'in order that', 'in case', 'in the event that', 'lest',
    'now that', 'once', 'only', 'only if', 'provided that', 'since', 'so',
    'supposing', 'that', 'than', 'though', 'till', 'unless', 'until', 'when',
    'whenever', 'where', 'whereas', 'wherever', 'whether or not', 'while']
    for i, sent in enumerate(sentences):
        sent = sent.lower().strip()
        sentences[i] = sent
        for sub in subordinates:
            splitted = sent.split(' ')
            if sub in splitted:
                idx = sent.find(sub)
                sentences[i] = (sent[:idx].strip() + ' ' + sent[idx + len(sub) + 1:].strip()).strip()
    cleaned = []
    for sent in sentences:
        if len(sent) > 0:
            cleaned.append(sent)
    return cleaned


def copy_insert(arr, val, idx):
    new = [v for v in arr]
    new[idx] = val
    return new


def remove_repeats(arr):
    new = []
    for val in arr:
        if len(new) == 0:
            new.append(val)
        elif new[-1] != val:
            new.append(val)
    return new


def lemmatize(app, data_type, data_idx, filter_func = lambda x : False):
    global app_stats, global_stats

    path = "Apps/%s/" % (app)
    library = os.listdir(path)

    total_reviews = 0
    num_skipped = 0
    num_sentences = 0

    target_reg = re.compile(r"%s_(.+)" % data_type)

    check_path(path + '/lemmatized/')

    for file in library:
        match = target_reg.match(file)
        if match:
            info = read_csv(path + file)

            final = [info[0]]
            for line in info[1:]:
                total_reviews += 1
                if filter_func(line):
                    num_skipped += 1
                    continue
                data = to_unicode(line[data_idx])
                sentences = sent_tokenize(data)
                filtered = filter_sentences(sentences)
                cleaned = remove_subordinates(filtered)
                for r in [c.lower().translate(None, '.?!@#$%^&*-+:') for c in cleaned]:
                    r = re.sub(' +', ' ', r).strip()
                    if len(r) > 0:
                        r = r.split(' ')
                        r = [word for word in r if ench.check(word)]
                        r = list(map(lemmatizer.lemmatize, r))
                        r = remove_repeats(r)
                        r = ' '.join(r)
                        nl = copy_insert(line, r, data_idx)
                        final.append(nl)
                        num_sentences += 1

            write_csv('Apps/%s/lemmatized/lemmatized_%s' % (app, file), final)

    data_type = data_type.capitalize()
    app_stats["1) Total Number of %s" % data_type] = total_reviews
    global_stats["1) Total Number of %s" % data_type] += total_reviews
    app_stats["2) %s Sentences Analyzed" % data_type] = num_sentences
    global_stats["2) %s Sentences Analyzed" % data_type] += num_sentences

    if data_type == 'Reviews':
        app_stats["1) Number of Reviews Discarded"] = num_skipped
        global_stats["1) Number of Reviews Discarded"] += num_skipped

        total_reviews *= 1.0
        percent_kept = ((total_reviews - num_skipped) / total_reviews) * 100
        app_stats["1) Percentage of Reviews Kept"] = "%.2f%%" % percent_kept

        total_reviews = global_stats["1) Total Number of Reviews"] * 1.0
        num_skipped = global_stats["1) Number of Reviews Discarded"]
        percent_kept = ((total_reviews - num_skipped) / total_reviews) * 100
        global_stats["1) Percentage of Reviews Kept"] = "%.2f%%" % percent_kept


def get_review_training_set():
    training_set = []

    path = "reviews_classified_lemmatized.csv"
    info = read_csv(path)

    for i in range(len(info)):
        try:
            review = info[i][-2]
            review = review.replace('  ', '')
            review = review.strip()
            category = info[i][-1]
            category = category.replace("'", '')
            category = category.replace(",", '')
            category = category.strip()
            if 'f' in category or 'b' in category:
                if 'f' in category:
                    training_set.append([review, 'f'])
                if 'b' in category:
                    training_set.append([review, 'b'])
            else:
                training_set.append([review, 'o'])
        except (KeyboardInterrupt):
            raise
        except:
            pass

    return training_set


def get_classifications(app, data_type, data_idx, date_idx, user_idx = None, rating_idx = None):
    global app_stats, global_stats

    path = "Apps/%s/lemmatized/" % (app)
    library = os.listdir(path)

    target_reg = re.compile(r"lemmatized_%s_(.+)" % data_type)

    training_set = get_review_training_set()
    NB_classifier = NB(train = training_set, alpha = 0.0001)
    SVM_classifier = SVM(train = training_set)

    check_paths('Apps/%s/NB/' % app, 'Apps/%s/SVM/' % app, 'Apps/%s/intersection/' % app)

    num_bugs = 0
    num_features = 0
    num_other = 0
    num_discarded = 0

    for file in library:
        match = target_reg.match(file)
        if match:
            info = read_csv(path + file)

            new_list = []
            NB_list = []
            SVM_list = []

            for i in range(1, len(info)):
                data = info[i][data_idx]
                date = info[i][date_idx]
                user = ''
                rating = ''
                if user_idx:
                    user = info[i][user_idx]
                if rating_idx:
                    rating = info[i][rating_idx]

                nb_res = NB_classifier.classify(Document(data))
                NB_list.append([data, nb_res, date, user, rating])

                svm_res = SVM_classifier.classify(Document(data))
                SVM_list.append([data, svm_res, date, user, rating])

                if nb_res == svm_res:
                    new_list.append([data, nb_res, date, user, rating])

                    if nb_res == 'b':
                        num_bugs += 1
                    elif nb_res == 'f':
                        num_features += 1
                    else:
                        num_other += 1
                else:
                    num_discarded += 1

            write_csv('Apps/%s/NB/NB_%s' % (app, file), NB_list)
            write_csv('Apps/%s/SVM/SVM_%s' % (app, file), SVM_list)
            write_csv('Apps/%s/intersection/intersection_%s' % (app, file), new_list)

    data_type = data_type.capitalize()
    app_stats["2) %s Classified as Features" % data_type] = num_features
    global_stats["2) %s Classified as Features" % data_type] += num_features
    app_stats["2) %s Classified as Bugs" % data_type] = num_bugs
    global_stats["2) %s Classified as Bugs" % data_type] += num_bugs
    app_stats["2) %s Classified as Neither" % data_type] = num_other
    global_stats["2) %s Classified as Neither" % data_type] += num_other
    app_stats["2) %s Not Classified" % data_type] = num_discarded
    global_stats["2) %s Not Classified" % data_type] += num_discarded


def get_individual_categories(app, data_type):
    path = "Apps/%s/intersection/" % app
    library = os.listdir(path)

    target_reg = re.compile(r"intersection_lemmatized_%s_(.+)" % data_type)

    b_list = []
    f_list = []

    for file in library:
        match = target_reg.match(file)
        if match:
            version = match.group(1)
            version = version[:version.rfind('.')]

            info = read_csv(path + file)

            for data, category, date, user, rating in info:
                category = category.replace('\r\n', '').strip()
                if category == 'b':
                    b_list.append([data, version, date, user, rating])
                elif category == 'f':
                    f_list.append([data, version, date, user, rating])

    check_path('Apps/%s/extracted/' % app)

    write_csv('Apps/%s/extracted/feature_%s.csv' % (app, data_type), f_list)
    write_csv('Apps/%s/extracted/bug_%s.csv' % (app, data_type), b_list)


def load_features(app_name):
    features = {}
    lines = read_csv('UI Topics/%s/%s_hdp_topics.csv' % (app, app))
    for line in lines[1:]:
        ui_element = line[3]
        word_pairs = line[4:]
        probs = []
        words = []
        for pair in word_pairs:
            splitted = pair.split('*')
            try:
                float(splitted[0])
            except ValueError:
                continue
            probs.append(float(splitted[0]))
            words.append(splitted[1])
        joined = []
        for i, prob in enumerate(probs):
            joined.append((words[i], prob))
        if ui_element not in features:
            features[ui_element] = []
        features[ui_element].append(joined)
    return features


def get_words(line):
    line = to_unicode(line)
    line = line.translate(None, string.punctuation)
    return [word.lower() for word in line.split()]


def find_topic_similarity(line, arr):
    words1 = list(set(get_words(line)))
    prob_sum = 0.0
    for word1 in words1:
        for word2, prob in arr:
            if word1 == word2:
                prob_sum += prob
    return prob_sum

def group_extracted(app, features):
    check_path('feature_groups/')
    path = "Apps/%s/extracted/feature_reviews.csv" % app
    info = read_csv(path)

    groups = {}
    for feature in features.keys():
        groups[feature] = []
    groups['OTHER'] = []

    for i in range(len(info)):
        line, version, date, user, rating = info[i]

        high_sim = 0
        high_feat = 'OTHER'
        for feature in features.keys():
            for arr in features[feature]:
                similarity = find_topic_similarity(line, arr) * 100
                if similarity > high_sim:
                    high_sim = similarity
                    high_feat = feature
        groups[high_feat].append((line, version, date, user, rating, high_sim))

    new_info = [["Feature", "Review", "Version", "Date", "User", "Rating", "Similarity"]]
    for key in groups.keys():
        for line, version, date, user, rating, similarity in groups[key]:
            new_info.append([key, line, version, date, user, rating, similarity])

    write_csv('feature_groups/groups_%s.csv' % app, new_info)

    counts = []
    counts.append(['feature', 'count'])
    for feat, vals in groups.items():
        counts.append([feat, len(vals)])
    write_csv('feature_groups/groups_counts_%s.csv' % app, counts)

    return groups


def find_implementation_versions(app, features):
    path = "Apps/%s/extracted/feature_commits.csv" % app
    info = read_csv(path)
    commits_read = True
    if len(info) == 0:
        commits_read = False
        info = read_csv("Apps/%s/extracted/feature_reviews.csv" % app)

    min_version = "NOT IMPLEMENTED"
    for _, version, _, _, _ in info:
        if version < min_version:
            min_version = version

    groups = {}
    for feature in features.keys():
        groups[feature] = []
    groups['OTHER'] = []

    x = []
    y = []
    if commits_read:
        for i in range(len(info)):
            line, version, date, _, _ = info[i]

            high_sim = 0
            high_feat = 'OTHER'
            for feature in features.keys():
                for arr in features[feature]:
                    similarity = find_topic_similarity(line, arr) * 100
                    if similarity > high_sim:
                        high_sim = similarity
                        high_feat = feature
            groups[high_feat].append((line, date, high_sim))

        array = []
        for key in sorted(groups.keys()):
            vals = sorted(groups[key], key=lambda x: x[2], reverse=True)
            
            y.append(key)
            if len(vals) > 0:
                array.append([key, vals[0][1], vals[0][0]])
                high_sim = vals[0][2]
                versions = sorted([version for _, version, sim in vals if sim == high_sim])
                x.append(versions[0])
            else:
                x.append(min_version)
        write_csv('feature_dates/featureImpDates_%s.csv' % app, array)
    else:
        for key in sorted(groups.keys()):
            y.append(key)
            x.append(min_version)
        
    return (x, y)


def find_all_versions(app):
    path = 'Apps/%s/' % app
    library = os.listdir(path)

    target_reg = re.compile(r"^(reviews|commits)_(.+)\.csv$")

    all_versions = set()
    for file in library:
        match = target_reg.match(file)
        if match:
            version = match.group(2)
            all_versions.add(version)
    return sort_versions(list(all_versions))


# imp_versions to number of weeks
def create_matrix(groups, app, imp_versions, imp_feats, versions):
    check_path('matricesL/')

    matrix = {}
    for feat in imp_feats:
        matrix[feat] = {}
        for version in versions:
            matrix[feat][version] = 0

    for feat, arr in groups.items():
        for val in arr:
            version = val[1]
            matrix[feat][version] += 1

    for i, feat in enumerate(imp_feats):
        imp_ver = imp_versions[i]
        matrix[feat][imp_ver] = "IMPLEMENTED"

    info = []
    temp = ['feature']
    for version in versions:
        temp.append(version)
    info.append(temp)

    for feat in imp_feats:
        temp = [feat]
        vers = matrix[feat]
        for version in versions:
            temp.append(vers[version])
        info.append(temp)

    write_csv('matricesL/%s_matrix.csv' % app, info)


'''Main Function'''
def main(app):
    global apps_skipped, app_stats
    if not os.path.exists('UI Topics/%s/%s_hdp_topics.csv' % (app, app)): # 'Apps/%s/features.txt' % app
        print("Skipping analysis of %s - no features found\n" % app)
        apps_skipped += 1
    else:
        app_stats = {}
        print("Starting analysis of %s" % app)

        print("Lemmatizing")
        lemmatize(app, 'reviews', -3, lambda x: x[4] != 'English')
        lemmatize(app, 'commits', 2)

        print("Classifying")
        get_classifications(app, 'reviews', -3, -6, -5, 6)
        get_classifications(app, 'commits', 2, 1)

        print("Separating Categories")
        get_individual_categories(app, 'reviews')
        get_individual_categories(app, 'commits')

        print("Grouping features")
        features = load_features(app)
        groups = group_extracted(app, features)
        imp_versions, imp_feats = find_implementation_versions(app, features)

        versions = find_all_versions(app)

        print("Creating matrix")
        create_matrix(groups, app, imp_versions, imp_feats, versions)

        stats = []
        for k, v in sorted(app_stats.items(), key=lambda x: x[0]):
            stats.append([k, v])
        write_csv("Apps/%s/stats.csv" % app, stats)

        print("Done analyzing %s\n" % app)

        apps_analyzed.append(app)


if __name__ == "__main__":
    apps = os.listdir("Apps/")
    for app in apps:
        main(app)

    global_stats["0) Total Apps"] = len(apps)
    global_stats["0) Apps Skipped"] = apps_skipped
    stats = []
    for k, v in sorted(global_stats.items(), key=lambda x: x[0]):
        stats.append([k, v])
    write_csv("global_stats.csv", stats)

    with open('analyzed_apps.txt', 'w') as f:
        f.writelines([app + "\n" for app in apps_analyzed])
