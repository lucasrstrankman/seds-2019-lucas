from utilities import *
from datetime import date
import math


STARTS_WITH = "intersection"
COMMIT_STARTS_WITH = "intersection_lemmatized_commits_"
REVIEW_STARTS_WITH = "intersection_lemmatized_reviews_"
FILE_NAME_LENGTH = len(COMMIT_STARTS_WITH)


PATH = 'Apps/'
FOLDER = "/" + STARTS_WITH + "/"

def convertCommitDate(day): # Because the commits and the reviews have their dates stored differently
    d1 = day.split('/')
    if len(d1[1]) == 1:
        d1[1] = "0" + d1[1]
    if len(d1[2]) == 1:
        d1[2] = "0" + d1[2]
    d = d1[0] + "-" + d1[1] + "-" + d1[2]
    return d

def countFeatureCommits(commits):
    fCount = 0
    for commit in commits:
        if commit[1] == "f":
            fCount += 1
    return fCount

def createPatterns(dates, reviews):
    patterns = []
    for i in range(len(dates)-1):        
        timeDif = date.fromisoformat(dates[i+1]) - (date.fromisoformat(dates[i]))

        if timeDif.days < 0:
            print("NEGATIVE DAYS")
        p = ""
        for review in reviews:
            if review[1] == "o":
                continue
            if dates[i] <= review[2] and review[2] < dates[i+1]:
                p += " " + review[1]
    
        patterns.append((p, timeDif.days))
    return patterns

        

def main():
    apps = os.listdir(PATH)
    allPatterns = []
    for app in apps:
        folderPath = PATH + app + FOLDER

        if (os.path.isdir(folderPath)): # If the directory with the reviews and commits exists, not all apps have it
            ### Get pairs of ReleaseCommits and ReleaseReviews
            lemmatized_files = os.listdir(folderPath)
            appCommits = []
            appReviews = []
            for file in lemmatized_files:
                if file.startswith(COMMIT_STARTS_WITH):
                    releaseCommits = read_csv(folderPath + file) 
                    for c in releaseCommits:
                        appCommits.append(c)
                elif (file.startswith(REVIEW_STARTS_WITH)): 
                    releaseReviews = []
                    with open(folderPath + file, 'rt', errors='ignore') as f:
                        reader = csv.reader(f)
                        releaseReviews = list(reader)           
                    for r in releaseReviews:
                        if (len(r) > 0):
                            appReviews.append(r)
                else:
                    print("ERROR - MISSED A FILE")
            dates = []
            for commit in appCommits:
                if (len(commit) > 0):
                    commit[2] = convertCommitDate(commit[2]) # Because the commits are stored as year/month/day instead of year-month-day
                    dates.append(commit[2])
            
            dates.sort()
            appReviews.sort(key = lambda x: x[2])

            patterns = createPatterns(dates, appReviews)
            for p in patterns:
                allPatterns.append(p)

    cleanedPatterns = []
    for p in allPatterns:
        if len(p[0]) > 0:
            cleanedPatterns.append(p)

    write_csv('RQ2.csv', cleanedPatterns)


if __name__ == '__main__':
    print("starting")
    main()