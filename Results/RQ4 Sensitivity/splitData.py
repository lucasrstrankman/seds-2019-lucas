import sys
import csv
from random import randrange

NUM_OF_FOLDS = 3

def crossValidationSplit(dataset, folds):
    dataset_split = list()
    dataset_copy = list(dataset)
    fold_size = int(len(dataset) / folds)

    for i in range(folds):
        fold = list()
        while len(fold) < fold_size:
            index = randrange(len(dataset_copy))
            fold.append(dataset_copy.pop(index))
        dataset_split.append(fold)
    return dataset_split


def createFiles(dataset_split):
    for i in range(len(dataset_split)):
        dataset_copy = list(dataset_split)
        dataset_copy.pop(i)

        data = []
        for dataset in dataset_copy:
            for pattern in dataset:
                data.append(pattern)
        
        path = "C:\\Users\\lucas\\Documents\\Research_project\\workingDir\\dataset_splits\\crossvalidation {index}.csv".format(index=i)
        print(path)
        with open(path, 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(data)

def main(argv):
    data = []

    with open(argv, 'rt') as f:
        reader = csv.reader(f)
        data = list(reader)


    dataset_split = crossValidationSplit(data, NUM_OF_FOLDS)
    createFiles(dataset_split)




if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print("Need 2 input files!")
        sys.exit()

    main(sys.argv[1])